package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class fragment_payCard extends Fragment {
    List list3;
    Spinner spinner;
    EditText amout;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_paycard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner = (Spinner) view.findViewById(R.id.spinnerAREAS);
        amout = (EditText) view.findViewById(R.id.editText3);
        setItemsOnSpinner3();

    }

    /*Fragment displays area spinner*/

    public void setItemsOnSpinner3() {
        list3 = new ArrayList<>();
        list3.add("Valitse alue:");
        list3.add("Finland");
        list3.add("Europe");
        list3.add("Asia");
        list3.add("North-America");
        list3.add("South-America");
        list3.add("Africa");
        list3.add("Oceania");
        list3.add("Worldwide");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list3);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }
}
