package com.example.harkkatyo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText kayttaja;
    EditText salasana;
    TextView virheilmoitus;
    ArrayList<User> users;

    //#E42E83 VÄRIKOODI

    /*When opening app, MainActivity opens database and reads XML to further use*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String accountCreated = "";
        kayttaja = (EditText) findViewById(R.id.editText);
        salasana = (EditText) findViewById(R.id.editText2);
        virheilmoitus = (TextView) findViewById(R.id.mistake);
        if (getIntent().hasExtra("message")) {
            accountCreated = getIntent().getStringExtra("message");
            Toast.makeText(MainActivity.this, accountCreated, Toast.LENGTH_SHORT).show();
        }

        Bank pankki = Bank.getBank();
        pankki.openDatabase(this);
        users = pankki.userList;
        pankki.readXML();
        pankki.entryXML.printXMLFile();

    }

    public void logIn(View v) {
        String user = kayttaja.getText().toString();
        String password = salasana.getText().toString();
        int a = 0;

        if (user.equals("admin") && password.equals("salasana")) {
            Intent intent = new Intent(MainActivity.this, adminApplication.class);
            startActivity(intent);
            finish();
        } else {
            //VERRATTAVA TIETO HAETAAN TIETOKANNASTA
            for (int i = 0; i < users.size(); i++) {
                if (user.equals(users.get(i).getUserID()) && password.equals(users.get(i).getPassword())) {
                    Intent intent = new Intent(MainActivity.this, confirm.class);
                    intent.putExtra("kayttaja", user);
                    startActivity(intent);
                    finish();
                    a = 1;
                }
            }
            if (a == 0) {
                virheilmoitus.setText("Väärä käyttäjätunnus tai salasana!");
            }
        }
    }

    public void signIn(View v) {
        Intent intent = new Intent(MainActivity.this, signIn.class);
        startActivity(intent);
    }
}
