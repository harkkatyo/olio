package com.example.harkkatyo;

public class User {
    String firstname;
    String lastname;
    String userID;
    String email;
    String phone;
    String password;

    public User (String newuserID, String newfirstname, String newlastname, String newemail, String newphone, String newpassword){
        this.firstname = newfirstname;
        this.lastname = newlastname;
        this.userID = newuserID;
        this.email = newemail;
        this.phone = newphone;
        this.password = newpassword;
    }

    public String getFirstName() {
        return firstname;
    }
    public String getLastName() {
        return lastname;
    }

    public String getUserID() {
        return userID;
    }
    public String getEmail() {
        return email;
    }

    public String getPhoneNo() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    /************************** METODIT USER-LUOKKAAN ASETUKSIA VARTEN: ********************************/

    public void setEmail(String newEmail) { this.email = newEmail;}
    public void setPhone(String newPhone) { this.phone = newPhone;}
    public void setPassword(String newPassword) { this.password = newPassword;}
}
