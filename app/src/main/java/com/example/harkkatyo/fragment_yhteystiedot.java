package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;
import java.util.regex.Pattern;

public class fragment_yhteystiedot extends Fragment {

    TextView textView;
    String id;
    Bank pankki = Bank.getBank();
    String email;
    String fn;
    String ln;
    String password;
    String phone;
    fragment_editContacts edit_obj;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        id = getArguments().getString("kayttajaID");
        View view = inflater.inflate(R.layout.layout_yhteystiedot, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textView = (TextView) view.findViewById(R.id.contactView);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Yhteystiedot");

        setText();

        Button buttonOpen = (Button) view.findViewById(R.id.edit);
        final Button buttonFragment = (Button) view.findViewById(R.id.updateButton);

        buttonOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment();
                buttonFragment.setVisibility(View.VISIBLE);
            }
        });


        buttonFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateContacts();
            }
        });
    }

    public void setText() {
        textView.setText(null);
        for (int i = 0; i < pankki.userList.size(); i++) {
            if (pankki.userList.get(i).getUserID().equals(id)) {
                email = pankki.userList.get(i).getEmail();
                phone = pankki.userList.get(i).getPhoneNo();
                fn = pankki.userList.get(i).getFirstName();
                ln = pankki.userList.get(i).getLastName();
                password = pankki.userList.get(i).getPassword();
            }
        }
        textView.setText(textView.getText().toString() + "\nEtunimi: " + fn + "\n");
        textView.setText(textView.getText().toString() + "\nSukunimi: " + ln + "\n");
        textView.setText(textView.getText().toString() + "\nPuhelinnumero: " + phone + "\n");
        textView.setText(textView.getText().toString() + "\nSähköposti: " + email + "\n");
    }

    public void openFragment() {
        Fragment fragment = null;
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        fragment = new fragment_editContacts();
        ft.replace(R.id.editFrame, fragment);
        ft.commit();

    }

    public void updateContacts() {
        FragmentManager manager = getChildFragmentManager();
        edit_obj = (fragment_editContacts) manager.findFragmentById(R.id.editFrame);
        String newEmail = edit_obj.newEmail.getText().toString();
        String newPhone = edit_obj.newPhone.getText().toString();
        String newPassword = edit_obj.newPassword.getText().toString();
        String check = edit_obj.checkPassword.getText().toString();
        boolean passed = false;

        if (!newEmail.equals("")) {
            passed = pankki.mDBHelper.userSetEmail(id, newEmail);
        }
        if (!newPhone.equals("")) {
            passed = pankki.mDBHelper.userSetPhone(id, newPhone);
        }
        if (!newPassword.equals("") && !check.equals("") && newPassword.equals(check)) {
            if (checkPassword(newPassword,check)) {
                passed = pankki.mDBHelper.userSetPassword(id, newPassword);
            }
        }
        if (passed){
            Toast.makeText(getActivity(), "Päivitys onnistui", Toast.LENGTH_SHORT).show();
        }
        setText();
    }

    /*Check if passwords matches and fills the requirements*/
    public boolean checkPassword(String salasana, String passwordAgain) {
        if (salasana.length() > 10) {
            if (Pattern.compile("[0-9]").matcher(salasana).find()) {
                if (Pattern.compile("[A-Z]").matcher(salasana).find()) {
                    if (salasana.equals(passwordAgain)) {
                        return true;
                    } else {
                        Toast.makeText(getActivity(),"SALASANAT EIVÄT TÄSMÄÄ",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else {
            Toast.makeText(getActivity(),"Salasanan täytyy olla yli 10 merkkiä pitkä sekä sisältää vähintään yksi numero ja yksi iso kirjain!",Toast.LENGTH_SHORT).show();
        }
        return false;
    }


}
