package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class fragment_talletus extends Fragment {
    String id;
    Bank pankki = Bank.getBank();
    Spinner spinner;
    List<String> accounts;
    EditText drawal;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        id = getArguments().getString("kayttajaID");
        return inflater.inflate(R.layout.layout_talletus, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Talleta rahaa");
        spinner = (Spinner) view.findViewById(R.id.spinner2);
        accounts = new ArrayList<>();

        /*Add accounts to spinner*/

        accounts.add("Valitse tili:");
        for (int i = 0; i < pankki.accountList.size(); i++) {
            if (id.equals(pankki.accountList.get(i).getUserID())){
                accounts.add(pankki.accountList.get(i).getAccountID());
            }
        }

        addItemsOnSpinner();

        Button button = (Button) view.findViewById(R.id.acceptLift);
        drawal = (EditText) view.findViewById(R.id.lift);

        /*On button click, put money on the account*/
        button.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                int position = spinner.getSelectedItemPosition();
                if (position != 0) {
                    String account =accounts.get(position);
                    String amoutString = drawal.getText().toString();
                    double amout = Double.parseDouble(amoutString);
                    pankki.takeOrPut(account, amout);
                    String message = "Rahaa talletettu " + amout;
                    Toast.makeText(getActivity(), message,  Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void addItemsOnSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, accounts);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String wantedAccount = accounts.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
