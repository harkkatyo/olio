package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.security.cert.Certificate;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class admin_yhteystiedot extends Fragment {
    Spinner spinner;
    TextView text;
    Bank pankki = Bank.getBank();
    String userID;
    CheckBox box;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.admin_yhteystiedot, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner = (Spinner) view.findViewById(R.id.SpinnerKäyttäjä);
        text = (TextView) view.findViewById(R.id.textView1200);
        box = (CheckBox) view.findViewById(R.id.checkBox2);
        text.setMovementMethod(new ScrollingMovementMethod());
        setItemsOnSpinner();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userID = spinner.getSelectedItem().toString();
                setText(userID);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button buttonOpen = (Button) view.findViewById(R.id.button6edit);
        final Button buttonFragment = (Button) view.findViewById(R.id.button5update);

        buttonOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment();
                buttonFragment.setVisibility(View.VISIBLE);
                box.setVisibility(View.VISIBLE);
            }
        });


        buttonFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateContacts(userID);
            }
        });
    }

    public void setItemsOnSpinner(){
        List users = new ArrayList();
        users.add("Valitse käyttäjä:");
        for (int i = 0; i < pankki.userList.size(); i++) {
            users.add(pankki.userList.get(i).getUserID());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, users);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public void setText(String userID) {
        text.setText(null);
        for (int i = 0; i < pankki.userList.size(); i++) {
            if (userID.equals(pankki.userList.get(i).getUserID())){
                text.setText(text.getText().toString() +"\n\nNimi:"+ pankki.userList.get(i).getFirstName() + " " + pankki.userList.get(i).getLastName() + "\n");
                text.setText(text.getText().toString()+ "Puhelinnumero: " + pankki.userList.get(i).getPhoneNo() + "\n");
                text.setText(text.getText().toString() + "Sähköposti: " +pankki.userList.get(i).getEmail()+"\n");
                text.setText(text.getText().toString() + "Salasana: " + pankki.userList.get(i).getPassword()+"\n");
            }
        }
    }

    public void openFragment() {
        Fragment fragment = null;
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        fragment = new fragment_editContacts();
        ft.replace(R.id.frameLayoutAdmin, fragment);
        ft.commit();

    }

    public void updateContacts(String userID) {
        FragmentManager manager = getChildFragmentManager();
        fragment_editContacts edit_obj = (fragment_editContacts) manager.findFragmentById(R.id.frameLayoutAdmin);
        String newEmail = edit_obj.newEmail.getText().toString();
        String newPhone = edit_obj.newPhone.getText().toString();
        String newPassword = edit_obj.newPassword.getText().toString();
        String check = edit_obj.checkPassword.getText().toString();
        boolean passed = false;

        if (!newEmail.equals("")) {
            passed = pankki.mDBHelper.userSetEmail(userID, newEmail);
        }
        if (!newPhone.equals("")) {
            passed = pankki.mDBHelper.userSetPhone(userID, newPhone);
        }
        if (!newPassword.equals("") && !check.equals("") && newPassword.equals(check)) {
            if (checkPassword(newPassword,check)) {
                passed = pankki.mDBHelper.userSetPassword(userID, newPassword);
            }
        }
        if (box.isChecked()) {
            pankki.mDBHelper.deleteContact(userID);
            setText(userID);
            passed = true;
        }
        if (passed) {
            Toast.makeText(getContext(), "Päivitys onnistui", Toast.LENGTH_SHORT).show();
        }
        setText(userID);
    }

    public boolean checkPassword(String salasana, String passwordAgain) {
        if (salasana.length() > 10) {
            if (Pattern.compile("[0-9]").matcher(salasana).find()) {
                if (Pattern.compile("[A-Z]").matcher(salasana).find()) {
                    if (salasana.equals(passwordAgain)) {
                        return true;
                    } else {
                        Toast.makeText(getActivity(),"SALASANAT EIVÄT TÄSMÄÄ",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else {
            Toast.makeText(getActivity(),"Salasanan täytyy olla yli 10 merkkiä pitkä sekä sisältää vähintään yksi numero ja yksi iso kirjain!",Toast.LENGTH_SHORT).show();
        }
        return false;
    }



}
