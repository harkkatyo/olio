package com.example.harkkatyo;


import android.content.Context;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/** This class creates the "Tilitapahtumat" XML file (userEntry.xml)
 * The Bank.java calls this class and sends the object list of ALL entries made by ALL users
 * in the bank application:
 * CreateEntryXML entryXML = new CreateEntryXML(entryList, this);
 *
 * The class also needs the context to be able to create the file.
 *
 * Before writing new entry nodes in the XML file, the XML file ALWAYS has to be read. Through
 * reading the file contents we get all the old entries back in the list of objects. If the XML
 * file is not first read, the old entries will be lost.
 *
 * entryList = entryXML.readAndWriteXMLFile(entryList);
 *
 * The method readAndWriteXMLFile first takes care of the old information before continuing
 * to the writeXMLFile -method.
 * */

public class CreateEntryXML {
    ArrayList<Entry> entryList;
    Context mContext;
    File file;
    String path;
    String filename;

    public CreateEntryXML(ArrayList<Entry> new_entryList, Context context) {
        entryList = new_entryList;
        mContext = context;
        filename = "tilitapahtumat.xml";
        path = mContext.getFilesDir() + "/" + filename;
        file = new File(path);
    }

    public void writeXMLFile(ArrayList<Entry> entryList) {
        try {
            FileOutputStream fos = new FileOutputStream(path);
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);

            xmlSerializer.startTag(null, "userEntries");

            /** The loop goes through the whole list of entries and writes them as nodes
             * in the XML file. */
            for (int i=0; i < entryList.size(); i++) {
                String dateTime = entryList.get(i).getDateTime();
                String sendAccount = entryList.get(i).getSendAccount();
                String getAccount = entryList.get(i).getGetAccount();
                String method = entryList.get(i).getMethod();
                String sum = entryList.get(i).getSum();

                /** NEW NOD */
                xmlSerializer.startTag(null, "entry");

                xmlSerializer.startTag(null, "datetime");
                xmlSerializer.text(dateTime);
                xmlSerializer.endTag(null, "datetime");

                xmlSerializer.startTag(null,"sendaccount");
                xmlSerializer.text(sendAccount);
                xmlSerializer.endTag(null, "sendaccount");

                xmlSerializer.startTag(null,"getaccount");
                xmlSerializer.text(getAccount);
                xmlSerializer.endTag(null, "getaccount");

                xmlSerializer.startTag(null,"method");
                xmlSerializer.text(method);
                xmlSerializer.endTag(null, "method");

                xmlSerializer.startTag(null,"sum");
                xmlSerializer.text(sum);
                xmlSerializer.endTag(null, "sum");

                xmlSerializer.endTag(null, "entry");
                /******************************************************/

            }

            xmlSerializer.endTag(null, "userEntries");

            xmlSerializer.endDocument();
            xmlSerializer.flush();
            String dataWrite = writer.toString();
            fos.write(dataWrite.getBytes());

            fos.close();
            System.out.println("The new entry has been written in the " + filename);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("File not found");
        }
        catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /** Before writing the XML file, the old contents have to be read into the entryList so any
     * information won't be lost and writing the file again and again stays problem-free. */
    public ArrayList<Entry> readAndWriteXMLFile(ArrayList<Entry> entryList) {
        try {
            if (file.exists()) { // The XML file can be read only if it's once been created already
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new FileInputStream(path)));
                //Document doc = dBuilder.parse(file);

                Element element = doc.getDocumentElement();
                element.normalize();

                NodeList nList = doc.getElementsByTagName("entry");
                for (int i = 0; i < nList.getLength(); i++) {

                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element2 = (Element) node;

                        String dateTime = getValue("datetime", element2);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                        Date date = dateFormat.parse(dateTime);

                        String sendAccount = getValue("sendaccount", element2);
                        String getAccount = getValue("getaccount", element2);
                        String method = getValue("method", element2);

                        String sum = getValue("sum", element2);
                        double dSum = Double.parseDouble(sum);
                        Entry entry = new Entry(date, sendAccount, getAccount, method, dSum);
                        entryList.add(entry); // The content of the XML-file is added to the list of entry objects for further use
                    }
                }
            }
            writeXMLFile(entryList); // In the end of reading the file, the contents will be written in the XML file.
        } catch (Exception e) {e.printStackTrace();}

        return entryList;
    }

    public void printXMLFile() {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new FileInputStream(file.getPath())));

            Element element = doc.getDocumentElement();
            element.normalize();
            NodeList nList = doc.getElementsByTagName("entry");
            System.out.println("************************** THE CONTENTS OF THE XML-FILE **********************************");
            for (int i = 0; i < nList.getLength(); i++) {

                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element2 = (Element) node;

                    String dateTime = getValue("datetime", element2);
                    String sendAccount = getValue("sendaccount", element2);
                    String getAccount = getValue("getaccount", element2);
                    String method = getValue("method", element2);
                    String sum = getValue("sum", element2);
                    /* These will be printed in Finnish to the user since the language of our app interface is Finnish. */
                    System.out.println("\nAika: " + dateTime + " Selite: " + method + "\n");
                    System.out.println(" Maksaja: " + sendAccount + " Saaja: " + getAccount);
                    System.out.println("Summa: " + sum + "\n____________________________");
                }
            }


        } catch (Exception e) {
            System.out.println("There is no transactions to print.");
        }

    }

    private static String getValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }


    /** As the first thing in the application we read the XML file and copy the entries to the
     * object list because the object list gets emptied between runs.
     * It's necessary to read it first so the user can look at the Tilitapahtuma information without
     * making a new entry first.
     * */
    public ArrayList<Entry> readEntriesToObjectList(ArrayList<Entry> entryList) {
        try {
            if (file.exists()) { // The XML file can be read only if it's once been created already
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(new InputSource(new FileInputStream(file.getPath())));

                Element element = doc.getDocumentElement();
                element.normalize();

                NodeList nList = doc.getElementsByTagName("entry");
                for (int i = 0; i < nList.getLength(); i++) {
                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element2 = (Element) node;

                        String dateTime = getValue("datetime", element2);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                        Date date = dateFormat.parse(dateTime);

                        String sendAccount = getValue("sendaccount", element2);
                        String getAccount = getValue("getaccount", element2);
                        String method = getValue("method", element2);

                        String sum = getValue("sum", element2);
                        double dSum = Double.parseDouble(sum);

                        Entry entry = new Entry(date, sendAccount, getAccount, method, dSum);
                        entryList.add(entry); // The content of the XML-file is added to the list of entry objects for further use
                    }
                }
                System.out.println("The XML file has been read and the entries have been added into the entryList.");
            }
            else {
                //System.out.println("The file does not exist. There's nothing to add to the entryList.");
            }
        } catch (FileNotFoundException e) {
            System.out.println("The file does not exist. There's nothing to add to the entryList.");} catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }


        return entryList;
    }
}

