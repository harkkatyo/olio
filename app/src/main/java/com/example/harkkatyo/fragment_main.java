package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class fragment_main extends Fragment {

    Bank pankki = Bank.getBank();
    ArrayList<Account> accountList = pankki.accountList;
    ArrayList<Bankcard> cardList = pankki.cardList;
    TextView viewA;
    String ID;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.layout_main, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Oma pankki");
        viewA = (TextView) view.findViewById(R.id.accountView);
        viewA.setMovementMethod(new ScrollingMovementMethod());

        ID = getArguments().getString("kayttajaID");

        setText();

        /*Listeners to create account and create bankcard*/
        Button button = (Button) view.findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), accountCreation.class);
                intent.putExtra("kayttaja", ID);
                startActivity(intent);
            }
        });

        Button buttonCard = (Button) view.findViewById(R.id.addCard);
        buttonCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BankcardCreation.class);
                intent.putExtra("kayttaja", ID);
                startActivity(intent);
            }
        });

    }

    /*Display all accounts and cards from user*/
    public void setText() {
        DecimalFormat df = new DecimalFormat("0.00");
        viewA.setText(viewA.getText().toString() + "Tilit:\n");
        for (int i=0; i < accountList.size(); i++) {
            if (ID.equals(accountList.get(i).getUserID())) {
                String newBalance = df.format(accountList.get(i).balance);
                viewA.setText(viewA.getText().toString() +accountList.get(i).type +  " "+ accountList.get(i).getAccountID() + "    Saldo: "+newBalance+"\n" );
            }
        }

        viewA.setText(viewA.getText().toString() + "\n\nKortit:\n");
        for (int i=0; i < accountList.size(); i++) {
            if (ID.equals(accountList.get(i).getUserID())) {
                String accountid = accountList.get(i).getAccountID();
                for (int a=0; a < cardList.size();a++) {
                    if (accountid.equals(cardList.get(a).getAccountID())) {
                        viewA.setText(viewA.getText().toString() + cardList.get(a).getCardNo() + " (pin: " + cardList.get(a).getPinCode() + ")\n");
                    }
                }
            }
        }

    }
}
