package com.example.harkkatyo;


public class Bankcard {
    String cardNo;
    String pinCode;
    double cashLimit;
    double payLimit;
    String area;
    String accountID;

    public Bankcard(String new_cardNo, String new_pin, double new_cash, double new_pay, String new_area, String new_account) {
        cardNo = new_cardNo;
        pinCode = new_pin;
        cashLimit = new_cash;
        payLimit = new_pay;
        area = new_area;
        accountID = new_account;
    }
    public String getCardNo() { return cardNo;}
    public String getPinCode() { return pinCode;}
    public double getCashLimit() { return cashLimit;}
    public double getPayLimit() { return payLimit; }
    public String getArea() { return area; }
    public String getAccountID() { return accountID; }

    public void setArea(String area) {
        this.area = area;
    }

    public void setCashLimit(double cashLimit) {
        this.cashLimit = cashLimit;
    }

    public void setPayLimit(double payLimit) {
        this.payLimit = payLimit;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
}

