package com.example.harkkatyo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BankcardCreation extends AppCompatActivity {
    EditText nosto;
    EditText maksu;
    EditText PIN;
    Spinner spinner;
    Button button;
    List list;
    Bank pankki = Bank.getBank();
    Spinner spinner2;
    String id;
    List accountlist;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bankcard_creation);
        nosto = (EditText) findViewById(R.id.nostoraja);
        maksu = (EditText) findViewById(R.id.maksuraja);
        PIN = (EditText) findViewById(R.id.pinkoodi);
        spinner = (Spinner) findViewById(R.id.areaSpinner);
        spinner2 = (Spinner) findViewById(R.id.accountSpinner);
        button = (Button) findViewById(R.id.createCard);
        if (getIntent().hasExtra("kayttaja")) {
            id = getIntent().getStringExtra("kayttaja");
            intent = new Intent(BankcardCreation.this, userApplication.class);
        }
        if (getIntent().hasExtra("from")) {
            intent = new Intent(BankcardCreation.this, adminApplication.class);
        }

        addItemsOnSpinner();
        addItemsOnSpinner2();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nostoraja = nosto.getText().toString();
                String maksuraja = maksu.getText().toString();
                String pinkoodi = PIN.getText().toString();
                String alue = spinner.getSelectedItem().toString();
                String account = spinner2.getSelectedItem().toString();
                if (nostoraja.equals("") || maksuraja.equals("") || pinkoodi.equals("") || alue.equals("Valitse toimivuusalue:") || account.equals("Valitse liitettävä tili:")) {
                    Toast.makeText(getApplicationContext(), "Joku valinta puuttuu",Toast.LENGTH_SHORT).show();
                } else {
                    boolean passed = pankki.createBankcard(alue, nostoraja, maksuraja, account, pinkoodi);
                    if (passed) {
                        intent.putExtra("kayttaja", id);
                        intent.putExtra("message", "Kortti luotu onnistuneesti!");
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Kortin luominen epäonnistui!",Toast.LENGTH_SHORT).show();
                    }
                }
            }


        });
    }

    /*Add areas to spinner*/

    public void addItemsOnSpinner(){
        list = new ArrayList<>();
        list.add("Valitse toimivuusalue:");
        list.add("Finland");
        list.add("Europe");
        list.add("Asia");
        list.add("North-America");
        list.add("South-America");
        list.add("Africa");
        list.add("Oceania");
        list.add("Worldwide");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /*Add accounts to spinner*/

    public void addItemsOnSpinner2() {
        accountlist = new ArrayList<>();
        accountlist.add("Valitse liitettävä tili:");
        for (int i = 0; i < pankki.accountList.size(); i++) {
            if (id.equals(pankki.accountList.get(i).getUserID())){
                if (!pankki.accountList.get(i).getType().equals("Saving")) {
                    accountlist.add(pankki.accountList.get(i).getAccountID());
                }
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, accountlist);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
    }
}
