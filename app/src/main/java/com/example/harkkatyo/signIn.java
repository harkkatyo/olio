package com.example.harkkatyo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

public class signIn extends AppCompatActivity {
    EditText firstname;
    EditText lastname;
    EditText phone;
    EditText email;
    EditText userID;
    EditText password;
    EditText passwordAgain;
    TextView error;
    DatabaseHelper mDBHelper;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        mDBHelper = new DatabaseHelper(this);
        firstname = (EditText) findViewById(R.id.firstname);
        lastname = (EditText) findViewById(R.id.lastname);
        phone = (EditText) findViewById(R.id.phone);
        email = (EditText) findViewById(R.id.email);
        userID = (EditText) findViewById(R.id.userID);
        password= (EditText) findViewById(R.id.password);
        passwordAgain = (EditText) findViewById(R.id.passwordcheck);
        error = (TextView) findViewById(R.id.error);
        if (getIntent().hasExtra("message")) {
            intent = new Intent(signIn.this, adminApplication.class);

        } else {
            intent = new Intent(signIn.this, MainActivity.class);
        }


    }


    /*Takes inputs from edittexts and checks they are not empty. After that creates new user to database.
    If user is created successfully, return to MainActivity and shows text*/

    public void createUser(View v) {

        String salasana = password.getText().toString();
        String fn = firstname.getText().toString();
        String ln = lastname.getText().toString();
        String ph = phone.getText().toString();
        String em = email.getText().toString();
        String id = userID.getText().toString();
        boolean passed;

        Boolean pass = checkfields(fn, ln, ph, em, id);
        if (pass == true) {
            Boolean right = checkPassword(salasana);
            if (right == true) {
                Bank pankki = Bank.getBank();
                passed = pankki.insertUser(id, salasana, pankki.getBankname(), fn, ln, ph, em);

                if (passed) {
                    intent.putExtra("message", "Tunnus luotu onnistuneesti!");
                    startActivity(intent);
                    finish();
                }
            }
        }

    }

    /* Checks field aren't empty*/

    public boolean checkfields(String fn, String ln, String ph, String em, String id){
        if (!fn.equals("")) {
            if (!ln.equals("")) {
                if(!ph.equals("")) {
                    if(!em.equals("")) {
                        if(!id.equals("")) {
                            return true;
                        }else {
                            error.setText("Henkilötunnus puuttuu!");
                        }
                    } else {
                        error.setText("Sähköposti puuttuu!");
                    }
                } else {
                    error.setText("Puhelinnumero puuttuu!");
                }
            } else {
                error.setText("Sukunimi puuttuu!");
            }
        } else {
            error.setText("Etunimi puuttuu!");
        }
        return false;
    }

    /* Checks password fills the requirements*/

    public boolean checkPassword(String salasana) {
            if (salasana.length() > 10) {
                if (Pattern.compile("[0-9]").matcher(salasana).find()) {
                    if (Pattern.compile("[A-Z]").matcher(salasana).find()) {
                        if (salasana.equals(passwordAgain.getText().toString())) {
                            return true;
                        } else {
                            error.setText("SALASANAT EIVÄT TÄSMÄÄ");
                        }
                    }
                }
            } else {
                error.setText("Salasanan täytyy olla yli 10 merkkiä pitkä sekä sisältää vähintään yksi numero ja yksi iso kirjain!");
            }
            return false;
        }

}
