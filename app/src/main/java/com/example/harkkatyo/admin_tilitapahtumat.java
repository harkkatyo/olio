package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class admin_tilitapahtumat extends Fragment {
        Spinner spinner;
        Spinner spinner2;
        TextView text;
        Bank pankki = Bank.getBank();
        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.admin_tilitapahtumat, container, false);
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            spinner2 = (Spinner) view.findViewById(R.id.tiliSpinner);
            spinner = (Spinner) view.findViewById(R.id.kayttajaSpinner);
            text = (TextView) view.findViewById(R.id.textView1200);
            text.setMovementMethod(new ScrollingMovementMethod());
            setItemsOnSpinner();

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    String userID = spinner.getSelectedItem().toString();
                    setItemsOnSpinner2(userID);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });

            spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!spinner2.getSelectedItem().toString().equals("Valitse tarkasteltava tili:")){
                        ArrayList actions = pankki.getTransactions(spinner2.getSelectedItem().toString());
                        printActions(actions);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }

        public void setItemsOnSpinner2(String userID){
            List accounts = new ArrayList();
            accounts.add("Valitse tarkasteltava tili:");
            for (int i = 0; i < pankki.accountList.size(); i++) {
                if (userID.equals(pankki.accountList.get(i).getUserID())){
                    accounts.add(pankki.accountList.get(i).getAccountID());
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, accounts);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(adapter);
        }

        public void printActions(ArrayList list) {
            text.setText(null);
            if (list.isEmpty()) {
                text.setText("\nEi tilitapahtumia");
            } else {
                for (int i = 0; i < list.size(); i++) {
                    text.setText(text.getText().toString() + "\n" + list.get(i) + "\n\n");
                }
            }
        }

        public void setItemsOnSpinner(){
            List users = new ArrayList();
            users.add("Valitse käyttäjä:");
            for (int i = 0; i < pankki.userList.size(); i++) {
                users.add(pankki.userList.get(i).getUserID());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, users);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        }

    }

