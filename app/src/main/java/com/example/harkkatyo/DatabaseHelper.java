package com.example.harkkatyo;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DatabaseHelper extends SQLiteOpenHelper{
    //The Android's default system path of your application database.
    //private static String DB_PATH = "/data/data/com.example.ht_sql_rajapinta/databases/";
    private static String DB_PATH = "";
    private static final String DATABASE_NAME = "bankdatabase.sqlite";
    private static final int DATABASE_VERSION = 2;

    /** The bank table */
    private static final String BANK_TABLE_NAME = "bank";
    private static final String BANK_COLUMN_BANKNAME = "bankname";
    /** The user table */
    private static final String USER_TABLE_NAME = "user";
    public static final String USER_COLUMN_ID = "userID";
    public static final String USER_COLUMN_PASSWORD = "password";
    public static final String USER_COLUMN_BANKNAME = "bankname";
    /** The contactinfo table */
    public static final String CONTACTS_TABLE_NAME = "contactinfo";
    public static final String CONTACTS_COLUMN_ID = "userID";
    public static final String CONTACTS_COLUMN_FIRSTNAME = "firstname";
    public static final String CONTACTS_COLUMN_LASTNAME = "lastname";
    public static final String CONTACTS_COLUMN_PHONENO = "phoneno";
    public static final String CONTACTS_COLUMN_EMAIL = "email";
    /** The bankaccount table */
    public static final String ACCOUNT_TABLE_NAME = "bankaccount";
    public static final String ACCOUNT_COLUMN_ID = "userID";
    public static final String ACCOUNT_COLUMN_ACCOUNT = "account";
    public static final String ACCOUNT_COLUMN_TYPE = "type"; // Type in (Credit, Debit, Saving)
    /** The debit table */
    public static final String DEBIT_TABLE_NAME = "debit";
    public static final String DEBIT_COLUMN_ACCOUNT = "account";
    public static final String DEBIT_COLUMN_BALANCE = "balance";
    /** The credit table */
    public static final String CREDIT_TABLE_NAME = "credit";
    public static final String CREDIT_COLUMN_ACCOUNT = "account";
    public static final String CREDIT_COLUMN_BALANCE = "balance";
    public static final String CREDIT_COLUMN_LIMIT = "creditlimit";
    /** The saving account table */
    public static final String SAVING_TABLE_NAME = "savingaccount";
    public static final String SAVING_COLUMN_ACCOUNT = "account";
    public static final String SAVING_COLUMN_BALANCE = "balance";
    public static final String SAVING_COLUMN_INTEREST = "interest";
    /** The bankcard table */
    public static final String CARD_TABLE_NAME = "bankcard";
    public static final String CARD_COLUMN_CARDNO = "cardno";
    public static final String CARD_COLUMN_PIN = "pincode";
    private static final String CARD_COLUMN_CASHLIMIT = "cashlimit";
    public static final String CARD_COLUMN_PAYLIMIT = "paymentlimit";
    public static final String CARD_COLUMN_AREA = "area";
    public static final String CARD_COLUMN_ACCOUNT = "account";



    private SQLiteDatabase mDataBase;
    private final Context mContext;
    private boolean mNeedUpdate = false;

    public Context context;
    public Context usercontext;
    Bank pankki = Bank.getBank();

    ArrayList<User> userList = new ArrayList<>();
    ArrayList<Account> accountList = new ArrayList<>();
    ArrayList<Bankcard> cardList = new ArrayList<>();

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null ,DATABASE_VERSION);

        if (android.os.Build.VERSION.SDK_INT >= 17)
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        else
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";

        this.mContext = context;


        copyDataBase();

        this.getReadableDatabase();
    }

    public void updateDataBase() throws IOException {
        if (mNeedUpdate) {
            File dbFile = new File(DB_PATH + DATABASE_NAME);
            if (dbFile.exists())
                dbFile.delete();

            copyDataBase();

            mNeedUpdate = false;
        }
    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DATABASE_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() {
        if (!checkDataBase()) {
            this.getReadableDatabase();
            this.close();
            try {
                copyDBFile();
            } catch (IOException mIOException) {
                throw new Error("ErrorCopyingDataBase");
            }
        }
    }

    private void copyDBFile() throws IOException {
        InputStream mInput = mContext.getAssets().open(DATABASE_NAME);
        //InputStream mInput = mContext.getResources().openRawResource(R.raw.info);
        OutputStream mOutput = new FileOutputStream(DB_PATH + DATABASE_NAME);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer)) > 0)
            mOutput.write(mBuffer, 0, mLength);
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }


    public boolean openDataBase() throws SQLException {
        mDataBase = SQLiteDatabase.openDatabase(DB_PATH + DATABASE_NAME, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        return mDataBase != null;
    }

    /** **************** BANK METHODS ****************
     * insertBank*/

    public boolean insertBank (String bankName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(BANK_COLUMN_BANKNAME, bankName);


        try {
            db.insertOrThrow(BANK_TABLE_NAME, null, contentValues);
            System.out.println("###########################\n "+ bankName +" has been added in the database.");
        } catch (SQLiteConstraintException ex) {
            System.out.println("###########################\n " + bankName + " is already in the database");
        }
        db.close();
        return true;
    }

    public void setUserContext(Context context) {
        usercontext = context;
    }


    /** **************** USER AND CONTACTINFO METHODS ****************
     * insertUser
     * createUserObject
     * putAllUsersInTheList
     * printUsersFromObjectList
     * getAllContacts
     * deleteContacts*/


    public boolean insertUser (String id, String passWord, String bankName, String firstName, String lastName, String phoneNo, String email, ArrayList<User> userList) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues userValues = new ContentValues();
        userValues.put(USER_COLUMN_ID, id);
        userValues.put(USER_COLUMN_PASSWORD, passWord);
        userValues.put(USER_COLUMN_BANKNAME, bankName);

        try {
            db.insertOrThrow(USER_TABLE_NAME, null, userValues);
            System.out.println("###########################\n The user with the userID " + id + " has been added in the database.");
            ContentValues contentValues = new ContentValues();
            contentValues.put(CONTACTS_COLUMN_ID, id);
            contentValues.put(CONTACTS_COLUMN_FIRSTNAME, firstName);
            contentValues.put(CONTACTS_COLUMN_LASTNAME, lastName);
            contentValues.put(CONTACTS_COLUMN_PHONENO, phoneNo);
            contentValues.put(CONTACTS_COLUMN_EMAIL, email);

            db.insertOrThrow(CONTACTS_TABLE_NAME, null, contentValues);
            System.out.println("###########################\n " + firstName + " has been added in the Contact database");
            createUserObject(id);

        } catch (SQLiteConstraintException ex) {
            System.out.println("###########################\n " + firstName + " is already in the database");
        } catch (NullPointerException ex) {
            System.out.print("Creating user failed");
        }

        db.close();
        return true;
    }

    /**In the createUserObject(id) function we look for the information of a single user (including
     * the contact information of the user) and make an object of the user.
     *
     * The object is then added in the userList.*/
    public ArrayList<User> createUserObject(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from user inner join contactinfo on user.userID = contactinfo.userID where user.userID = '"+id+"'", null );
        userObjectCursor(res);
        db.close();
        System.out.println(id+" was now added in the userList.");
        return userList;
    }


    /** When starting the app we first add all the users in the database into the list of objects */
    public ArrayList<User> putAllUsersInTheList() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from user inner join contactinfo on user.userID = contactinfo.userID", null );
        userObjectCursor(res);
        db.close();
        System.out.println("All users in the database have been now added in the object list.");
        return userList;
    }

    public void userObjectCursor(Cursor res) {
        if(res.moveToFirst()) {
            do {
                String userID = res.getString(res.getColumnIndex(CONTACTS_COLUMN_ID));
                String firstname = res.getString(res.getColumnIndex(CONTACTS_COLUMN_FIRSTNAME));
                String lastname = res.getString(res.getColumnIndex(CONTACTS_COLUMN_LASTNAME));
                String email = res.getString(res.getColumnIndex(CONTACTS_COLUMN_EMAIL));
                String phone = res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONENO));
                String pass = res.getString(res.getColumnIndex(USER_COLUMN_PASSWORD));
                userList.add(new User(userID, firstname, lastname, email, phone, pass));
            }
            while (res.moveToNext());
        }
        res.close();
    }



    public void printUsersFromObjectList() {
        for (int i=0; i < userList.size(); i++) {
            System.out.println("######################\n"+(i+1)+". Name: "+userList.get(i).getFirstName() + " Password: "+userList.get(i).getPassword()+"\n");
        }
    }



    public void deleteContacts () {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ USER_TABLE_NAME);
        System.out.println("*******************\nAll contacts are deleted.\n****************************");
    }

    public void deleteContact (String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ USER_TABLE_NAME +" where " + USER_COLUMN_ID + " = '" + id + "'");
        System.out.println("*******************\nContact is deleted.\n****************************");
    }



    /** **************** BANKACCOUNT METHODS ****************
     * insertAccount
     * createAccountObject
     * printAccountsFromObjectList*/



    public void insertAccount (String id, String type, String account, double balance, double interest, double limit) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(ACCOUNT_COLUMN_ID, id);
        contentValues.put(ACCOUNT_COLUMN_TYPE, type);
        contentValues.put(ACCOUNT_COLUMN_ACCOUNT, account);

        try {
            db.insertOrThrow(ACCOUNT_TABLE_NAME, null, contentValues);

            if (type.equals("Debit")) {
                ContentValues debitValues = new ContentValues();
                debitValues.put(DEBIT_COLUMN_ACCOUNT, account);
                debitValues.put(DEBIT_COLUMN_BALANCE, balance);
                db.insertOrThrow(DEBIT_TABLE_NAME, null, debitValues);
                System.out.println("###########################\n The account with the userID " + id + " and accountID "+account +" has been added in the debit.");
            }
            else if (type.equals("Credit")) {
                ContentValues creditValues = new ContentValues();
                creditValues.put(CREDIT_COLUMN_ACCOUNT, account);
                creditValues.put(CREDIT_COLUMN_BALANCE, balance);
                creditValues.put(CREDIT_COLUMN_LIMIT, limit);
                db.insertOrThrow(CREDIT_TABLE_NAME, null, creditValues);
                System.out.println("###########################\n The account with the userID " + id + " and accountID "+account +" has been added in the credit.");

            }
            else if (type.equals("Saving")) {
                ContentValues savingValues = new ContentValues();
                savingValues.put(SAVING_COLUMN_ACCOUNT, account);
                savingValues.put(SAVING_COLUMN_BALANCE, balance);
                savingValues.put(SAVING_COLUMN_INTEREST, interest);
                db.insertOrThrow(SAVING_TABLE_NAME, null, savingValues);
                System.out.println("###########################\n The account with the userID " + id + " and accountID "+account +" has been added in the saving accounts.");

            }
            createAccountObject(account, type);
        } catch (SQLiteConstraintException ex) {
            System.out.println("###########################\n Account " + account + " is already in the database");
        }

        db.close();
    }

    /**In the createAccountObject(id) function we look for the information of a single account
     * and make an object of the account, no matter which account type it is. There are
     * three cursors because all account types have their own tables.
     *
     * DEBIT: interest = 0, credit limit = 0
     * CREDIT: interest = 0, credit limit != 0
     * SAVING ACCOUNT: interest != 0, credit limit = 0
     *
     * The object is then added in the accountList.*/
    public void createAccountObject(String account, String type) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor debitC = db.rawQuery("select * from bankaccount inner join debit on bankaccount.account = debit.account where debit.account = '" + account + "'", null);
        Cursor creditC = db.rawQuery("select * from bankaccount inner join credit on bankaccount.account = credit.account where credit.account = '" + account + "'", null);
        Cursor savingC = db.rawQuery("select * from bankaccount inner join savingaccount on bankaccount.account = savingaccount.account where savingaccount.account = '" + account + "'", null);

        if (type.equals("Debit")) {
            if (debitC.moveToFirst()) {
                do {
                    String userID = debitC.getString(debitC.getColumnIndex(ACCOUNT_COLUMN_ID));
                    float balance= debitC.getFloat(debitC.getColumnIndex(DEBIT_COLUMN_BALANCE));
                    accountList.add(new Account(userID, account, type, balance, 0, 0));
                } while (debitC.moveToNext());
            }
            debitC.close();
        } else if (type.equals("Credit")) {
            if (creditC.moveToFirst()) {
                do {
                    String userID = creditC.getString(creditC.getColumnIndex(ACCOUNT_COLUMN_ID));
                    float balance= creditC.getFloat(creditC.getColumnIndex(CREDIT_COLUMN_BALANCE));
                    float limit = creditC.getFloat(creditC.getColumnIndex(CREDIT_COLUMN_LIMIT));
                    accountList.add(new Account(userID, account, type, balance, 0, limit));

                } while (creditC.moveToNext());
            }
            creditC.close();
        } else if (type.equals("Saving")) {

            if (savingC.moveToFirst()) {
                do {
                    String userID = savingC.getString(savingC.getColumnIndex(ACCOUNT_COLUMN_ID));
                    float balance= savingC.getFloat(savingC.getColumnIndex(SAVING_COLUMN_BALANCE));
                    float interest = savingC.getFloat(savingC.getColumnIndex(SAVING_COLUMN_INTEREST));
                    accountList.add(new Account(userID, account, type, balance, interest, 0));

                } while (savingC.moveToNext());
            }
            savingC.close();
        }


        db.close();
        System.out.println(account+" of type "+type+" was now added in the accountList.");
    }

    public ArrayList putAllAccountsInTheList() {
        SQLiteDatabase db = this.getReadableDatabase();

        String[] types= {"Credit", "Debit", "Saving"};

        for (String s:types) {

            //String type = accountC.getString(accountC.getColumnIndex(ACCOUNT_COLUMN_TYPE));
            Cursor debitC = db.rawQuery("select * from bankaccount inner join debit on bankaccount.account = debit.account", null);
            Cursor creditC = db.rawQuery("select * from bankaccount inner join credit on bankaccount.account = credit.account", null);
            Cursor savingC = db.rawQuery("select * from bankaccount inner join savingaccount on bankaccount.account = savingaccount.account", null);
            accountObjectCursor(s, debitC, creditC, savingC);
        }


        db.close();
        System.out.println("All accounts were now added in the accountList.");
        return accountList;
    }

    public ArrayList<Account> accountObjectCursor(String type, Cursor debitC, Cursor creditC, Cursor savingC) {
        if (type.equals("Debit")) {
            if (debitC.moveToFirst()) {
                do {
                    String userID = debitC.getString(debitC.getColumnIndex(ACCOUNT_COLUMN_ID));
                    String account =  debitC.getString(debitC.getColumnIndex(ACCOUNT_COLUMN_ACCOUNT));
                    float balance= debitC.getFloat(debitC.getColumnIndex(DEBIT_COLUMN_BALANCE));
                    accountList.add(new Account(userID, account, type, balance, 0, 0));
                } while (debitC.moveToNext());
            }
            debitC.close();
        } else if (type.equals("Credit")) {
            if (creditC.moveToFirst()) {
                do {
                    String userID = creditC.getString(creditC.getColumnIndex(ACCOUNT_COLUMN_ID));
                    String account = creditC.getString(creditC.getColumnIndex(ACCOUNT_COLUMN_ACCOUNT));
                    float balance= creditC.getFloat(creditC.getColumnIndex(CREDIT_COLUMN_BALANCE));
                    float limit = creditC.getFloat(creditC.getColumnIndex(CREDIT_COLUMN_LIMIT));
                    accountList.add(new Account(userID, account, type, balance, 0, limit));

                } while (creditC.moveToNext());
            }
            creditC.close();
        } else if (type.equals("Saving")) {
            if (savingC.moveToFirst()) {
                do {
                    String userID = savingC.getString(savingC.getColumnIndex(ACCOUNT_COLUMN_ID));
                    String account = savingC.getString(savingC.getColumnIndex(ACCOUNT_COLUMN_ACCOUNT));
                    float balance= savingC.getFloat(savingC.getColumnIndex(SAVING_COLUMN_BALANCE));
                    float interest = savingC.getFloat(savingC.getColumnIndex(SAVING_COLUMN_INTEREST));
                    accountList.add(new Account(userID, account, type, balance, interest, 0));

                } while (savingC.moveToNext());
            }
            savingC.close();
        }
        return accountList;
    }


    public void printAccountsFromObjectList() {
        for (int i=0; i < accountList.size(); i++) {
            System.out.println("######################\n"+(i+1)+". Type: "+accountList.get(i).getType() + " Account: "+accountList.get(i).getAccountID()+"\n");
            System.out.println("\tUserID: "+accountList.get(i).getUserID() + " Balance: "+accountList.get(i).getBalance()+"\n");
        }
    }

    public void printOneUserObject(String userID) {
        for (int i=0; i < userList.size(); i++) {
            if ((userList.get(i).getUserID()).equals(userID)) {
                System.out.println(". UserID: " + userList.get(i).getUserID() + " Name: " + userList.get(i).getFirstName() + " Password: " + userList.get(i).getPassword() + "\n");
                System.out.println("Email: " + userList.get(i).getEmail() + " Phone number: " + userList.get(i).getPhoneNo());
                break;
            }
        }
    }


    public void deleteAccount (String account) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i=0; i < accountList.size(); i++) {
            if (accountList.get(i).getAccountID().equals(account)) {
                accountList.remove(i);
                db.execSQL("delete from "+ ACCOUNT_TABLE_NAME +" where "+ACCOUNT_COLUMN_ACCOUNT+" = '"+ account +"'");
                System.out.println("*******************\n The account "+account+" was deleted. \n****************************");
                break;
            }
        }
        db.close();
    }

    /*********************** THE USER CAN USE THESE METHODS *******************************/

    /** takeOrPutCash is a method that can be used for both PUTTING and TAKING cash out of the bank
     * account.
     * When the user wants to PUT cash on the bank account, the sum (double cash) has to be
     * positive.
     * When the user is TAKING cash out of the bank account, the sum is negative.
     * The method updates the balance of the bank account both in the database and in the object.
     * */
    public ArrayList<Entry> takeOrPutCash(String account, double cash, ArrayList<Entry> entryList) {
        Entry entry = null;
        String strSQL;
        String accountID;
        String type;
        double limit;
        double balance;
        double newBalance;
        String method = "-";
        SQLiteDatabase db = this.getWritableDatabase();

        if (cash > 0) {
            method = "Tilitalletus";
        }
        else if (cash < 0) {
            method = "Tilinosto";
        }
        else {
            method = "Tyhjä tapahtuma";
        }

        /* For loop to go through the object list of all accounts. The purpose is to find the
         * type and the balance of the right account.*/
        for (int i=0; i < accountList.size(); i++) {
            accountID = accountList.get(i).getAccountID();
            balance = accountList.get(i).getBalance();
            type = accountList.get(i).getType();
            limit = accountList.get(i).getLimit();

            if (accountID.equals(account)) {
                System.out.println("Type: " + type + " Account: " + accountID + " Balance: " + balance + "\n");
                newBalance = balance + cash; // cash is negative if money is taken out, positive if money is added

                if (type.equals("Debit")) {
                    if (balance >= (-cash)) {
                        strSQL = "UPDATE " + DEBIT_TABLE_NAME + " SET " + DEBIT_COLUMN_BALANCE + " = " + newBalance + " WHERE " + DEBIT_COLUMN_ACCOUNT + " = '" + account + "'";
                        db.execSQL(strSQL);
                        accountList.get(i).setBalance(cash);
                        /** Create an entry object */
                        Date dateTime = new Date();
                        entry = new Entry(dateTime, account, "-", method, cash);
                        entryList.add(entry);

                        System.out.println("The new balance of the account "+account+" is "+newBalance);
                        Toast.makeText(usercontext, "Toiminto onnistui", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        System.out.println("The balance of this account is "+balance+". It's not possible to proceed the withdrawal.");
                        Toast.makeText(usercontext, "Toiminto epäonnistui, tarkista saldo", Toast.LENGTH_SHORT).show();
                    }
                }
                else if (type.equals("Credit")) {
                    if ((balance+cash) >= (-limit)) {
                        strSQL = "UPDATE " + CREDIT_TABLE_NAME + " SET " + CREDIT_COLUMN_BALANCE + " = " + newBalance + " WHERE " + CREDIT_COLUMN_ACCOUNT + " = '" + account + "'";
                        db.execSQL(strSQL);
                        accountList.get(i).setBalance(cash);
                        /** Create an entry object */
                        Date dateTime = new Date();
                        entry = new Entry(dateTime, account, "-", method, cash);
                        entryList.add(entry);
                        System.out.println("The new balance of the account "+account+" is "+newBalance);
                        Toast.makeText(usercontext, "Toiminto onnistui", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        System.out.println("The credit limit of this account is "+limit+". It's not possible to proceed the withdrawal.");
                        Toast.makeText(usercontext, "Toiminto epäonnistui, tarkista luottoraja", Toast.LENGTH_SHORT).show();
                    }

                }
                else if (type.equals("Saving")){
                    if (cash > 0) { // The user can only ADD money on the saving account so the cash has to be positive.
                        strSQL = "UPDATE " + SAVING_TABLE_NAME + " SET " + SAVING_COLUMN_BALANCE + " = " + newBalance + " WHERE " + SAVING_COLUMN_ACCOUNT + " = '" + account + "'";
                        db.execSQL(strSQL);
                        accountList.get(i).setBalance(cash);
                        /** Create an entry object */
                        Date dateTime = new Date();
                        entry = new Entry(dateTime, account, "-", method, cash);
                        entryList.add(entry);
                        System.out.println("The new balance of the account "+account+" is "+newBalance);
                        Toast.makeText(usercontext, "Talletus onnistui", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        System.out.println("The account is of the saving type which can not be taken money out of.");
                    }

                }

            }
        }


        db.close();
        return entryList;
    }
    /** transferMoney method is for transferring money between accounts.
     * sendAccount is the account that transfers the money
     * getAccount is the receiver of the money
     * */
    public ArrayList<Entry> transferMoney(String sendAccount, String getAccount, double sum, ArrayList<Entry> entryList) {
        Entry entry = null;
        String strSQL;
        String getSQL;
        String GET_TABLE_NAME = "-";
        String method = "Tilisiirto";

        int sendAccountIndex = -1;
        String sendType = "-";
        double sendLimit = 0;
        double sendBalance = 0;
        double newSendBalance;

        int getAccountIndex = -1;
        String getType = "-";
        double getBalance = 0;
        double newGetBalance;

        SQLiteDatabase db = this.getWritableDatabase();

        /* For loop to go through the object list of all accounts. The purpose is to find both
         * accounts on the list of objects to be able to make changes to their balance later. */

        for (int i=0; i < accountList.size(); i++) {
            if ((accountList.get(i).getAccountID()).equals(getAccount)) {
                getAccountIndex = i; // The index of the getAccount on the list of objects
                getBalance = accountList.get(i).getBalance();
                getType = accountList.get(i).getType();
            }
            else if ((accountList.get(i).getAccountID()).equals(sendAccount)) {
                /* The needed variables of the sending account */
                sendBalance = accountList.get(i).getBalance();
                sendType = accountList.get(i).getType();
                sendLimit = accountList.get(i).getLimit();
                sendAccountIndex = i;

                System.out.println("The type of the sending account: " + sendType + " Account: " + sendAccount + " Balance: " + sendBalance + "\n");
            }
        }

        /* If both of the accounts were found on the list of objects, they get new indexes that are different from -1.
        Also they can't be the same object.        */

        if ((getAccountIndex != -1) && (sendAccountIndex != -1) && (getAccountIndex != sendAccountIndex)) {
            newSendBalance = sendBalance - sum; // The sum has to be positive
            newGetBalance = getBalance + sum;

            if (getType.equals("Debit")) {
                GET_TABLE_NAME = DEBIT_TABLE_NAME;
            }
            else if (getType.equals("Credit")) {
                GET_TABLE_NAME = CREDIT_TABLE_NAME;
            }
            else if (getType.equals("Saving")) {
                GET_TABLE_NAME = SAVING_TABLE_NAME;
            }

            if (sendType.equals("Debit")) {
                if (sendBalance >= (sum)) {
                    strSQL = "UPDATE " + DEBIT_TABLE_NAME + " SET " + DEBIT_COLUMN_BALANCE + " = " + newSendBalance + " WHERE " + DEBIT_COLUMN_ACCOUNT + " = '" + sendAccount + "'";
                    db.execSQL(strSQL);
                    getSQL = "UPDATE " + GET_TABLE_NAME + " SET balance = " + newGetBalance + " WHERE account = '" + getAccount + "'";
                    db.execSQL(getSQL);
                    accountList.get(sendAccountIndex).setBalance(-sum);
                    accountList.get(getAccountIndex).setBalance(sum);
                    /** Create an entry object */
                    Date dateTime = new Date();
                    entry = new Entry(dateTime, sendAccount, getAccount, method, sum);
                    entryList.add(entry);
                    System.out.println("The new balance of the account " + sendAccount + " is " + newSendBalance);
                    Toast.makeText(usercontext, "Toiminto onnistui", Toast.LENGTH_SHORT).show();
                } else {
                    System.out.println("The balance of this account is " + sendBalance + ". It's not possible to proceed the withdrawal.");
                    Toast.makeText(usercontext, "Toiminto epäonnistui, tarkista saldo ja tiedot", Toast.LENGTH_SHORT).show();
                }
            } else if (sendType.equals("Credit")) {
                if ((sendBalance - sum) >= (-sendLimit)) {
                    strSQL = "UPDATE " + CREDIT_TABLE_NAME + " SET " + CREDIT_COLUMN_BALANCE + " = " + newSendBalance + " WHERE " + CREDIT_COLUMN_ACCOUNT + " = '" + sendAccount + "'";
                    db.execSQL(strSQL);
                    getSQL = "UPDATE " + GET_TABLE_NAME + " SET balance = " + newGetBalance + " WHERE account = '" + getAccount + "'";
                    db.execSQL(getSQL);
                    accountList.get(sendAccountIndex).setBalance(-sum);
                    accountList.get(getAccountIndex).setBalance(sum);
                    /** Create an entry object */
                    Date dateTime = new Date();
                    entry = new Entry(dateTime, sendAccount, getAccount, method, sum);
                    entryList.add(entry);
                    System.out.println("The new balance of the account " + sendAccount + " is " + newSendBalance);
                    Toast.makeText(usercontext, "Toiminto onnistui", Toast.LENGTH_SHORT).show();
                } else {
                    System.out.println("The credit limit of this account is " + sendLimit + ". It's not possible to proceed the withdrawal.");
                    Toast.makeText(usercontext, "Toiminto epäonnistui, tarkista tiedot ja saldo", Toast.LENGTH_SHORT).show();
                }

            } else if (sendType.equals("Saving")) {
                System.out.println("The account is of the saving type which can not be transferred money out of.");
            }
        }
        else {
            System.out.println("One or both of the accounts do not exist OR the transfer is illegal.");
            Toast.makeText(usercontext, "Vastaanottajaa ei löydy!", Toast.LENGTH_SHORT).show();

        }
        db.close();
        return entryList;
    }

    public boolean userSetEmail(String userID, String newEmail) {
        /* These variables are the original variables of the user object. */
        String oUserID;
        String oEmail;
        boolean passed = false;

        String strSQL;
        SQLiteDatabase db = this.getWritableDatabase();

        strSQL = "UPDATE " + CONTACTS_TABLE_NAME + " SET " + CONTACTS_COLUMN_EMAIL + " = '" + newEmail + "' WHERE " + CONTACTS_COLUMN_ID + " = '" + userID + "'";
        db.execSQL(strSQL); // Update the email in the database

        /* For loop to go through the object list of all users. The purpose is to find the
         * right object to be able to switch the email of the object, too.*/
        for (int i=0; i < userList.size(); i++) {
            oUserID = userList.get(i).getUserID();
            oEmail = userList.get(i).getEmail();

            if (oUserID.equals(userID)) {
                userList.get(i).setEmail(newEmail); // Update the email in the list of objects
                System.out.println("The user "+  userID +" switches the email from "+oEmail+" to "+newEmail);
                passed = true;
            }
        }
        db.close();
        return passed;
    }

    public boolean userSetPhone(String userID, String newPhone) {
        /* These variables are the original variables of the user object. */
        String oUserID;
        String oPhone;
        boolean passed = false;

        String strSQL;
        SQLiteDatabase db = this.getWritableDatabase();

        strSQL = "UPDATE " + CONTACTS_TABLE_NAME + " SET " + CONTACTS_COLUMN_PHONENO + " = '" + newPhone + "' WHERE " + CONTACTS_COLUMN_ID + " = '" + userID + "'";
        db.execSQL(strSQL); // Update the phone number in the database

        /* For loop to go through the object list of all users. The purpose is to find the
         * right object to be able to switch the phone number of the object, too.*/
        for (int i=0; i < userList.size(); i++) {
            oUserID = userList.get(i).getUserID();
            oPhone = userList.get(i).getPhoneNo();

            if (oUserID.equals(userID)) {
                userList.get(i).setPhone(newPhone);// Update the phone number in the list of objects
                System.out.println("The user "+  userID +" switches the phone number from "+oPhone+" to "+newPhone);
                passed = true;
            }
        }
        db.close();
        return passed;
    }

    public boolean userSetPassword(String userID, String newPassword) {
        /* These variables are the original variables of the user object. */
        String oUserID;
        String oPassword;
        boolean passed = false;

        String strSQL;
        SQLiteDatabase db = this.getWritableDatabase();

        strSQL = "UPDATE " + USER_TABLE_NAME + " SET " + USER_COLUMN_PASSWORD + " = '" + newPassword + "' WHERE " + CONTACTS_COLUMN_ID + " = '" + userID + "'";
        db.execSQL(strSQL); // Update the phone number in the database

        /* For loop to go through the object list of all users. */
        for (int i=0; i < userList.size(); i++) {
            oUserID = userList.get(i).getUserID();
            oPassword = userList.get(i).getPassword();

            if (oUserID.equals(userID)) {
                userList.get(i).setPassword(newPassword); // Update the password of the object
                System.out.println("The user "+  userID +" switches the password from "+oPassword+" to "+newPassword);
                passed = true;
            }
        }
        db.close();
        return passed;
    }


    /******************************* BANKCARD METHODS *********************************/
    /** insertBankCard
     * createCardObject
     * putAllCardsInTheList
     * cardObjectCursor
     * printCardsFromObjectList*/

    /** In the method insertBankCard we create a new bankcard and add it in the bankcard table in the database. After that we call a function
     * which adds the new bankcard on the list of objects (cardList).
     * The bankcard has to be linked to an existing bank account which can't be of type "Saving". */
    public boolean insertBankCard (String cardNo, String pin, double cashLimit, double payLimit, String area, String account) {
        SQLiteDatabase db = this.getWritableDatabase();
        String accountType = "-";
        boolean passed = false;

        for (int i=0; i < accountList.size(); i++) {
            String objAccount = accountList.get(i).getAccountID();
            if (objAccount.equals(account)) {
                accountType = accountList.get(i).getType();
            }
        }
        if (accountType.equals("Debit") || accountType.equals("Credit")) { // The user can only add bankcards on accounts that are either Debit or Credit
            ContentValues cardValues = new ContentValues();
            cardValues.put(CARD_COLUMN_CARDNO, cardNo);
            cardValues.put(CARD_COLUMN_PIN, pin);
            cardValues.put(CARD_COLUMN_CASHLIMIT, cashLimit);
            cardValues.put(CARD_COLUMN_PAYLIMIT, payLimit);
            cardValues.put(CARD_COLUMN_AREA, area);
            cardValues.put(CARD_COLUMN_ACCOUNT, account);

            try {
                db.insertOrThrow(CARD_TABLE_NAME, null, cardValues);
                createCardObject(cardNo);
                passed = true;
            } catch (SQLiteConstraintException ex) {
                System.out.println("###########################\n Bankcard" + cardNo + " is already in the database");
            }
        }
        else if (accountType.equals("Saving")) {
            System.out.println("One can not link a bank card to a saving account.");
        }
        else {
            System.out.println("No such account found.");
        }

        db.close();
        return passed;
    }

    /** When we add a new bankcard into the database, we also create an object of it. This object
     * is then added in the cardList. */
    public void createCardObject(String cardNo) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from bankcard where bankcard.cardno = '"+cardNo+"'", null );
        cardObjectCursor(res);
        db.close();
        res.close();
        System.out.println(cardNo+" was now added in the cardList.");
    }
    /** When starting the app we first copy all the cards from the database into the list of objects */
    public ArrayList putAllCardsInTheList() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from bankcard", null );
        cardList = cardObjectCursor(res);
        db.close();
        res.close();
        System.out.println("All bankcards in the database have now been added in the object list.");
        return cardList;
    }

    public ArrayList cardObjectCursor(Cursor res) {
        if(res.moveToFirst()) {
            do {
                String cardno = res.getString(res.getColumnIndex(CARD_COLUMN_CARDNO));
                String pin = res.getString(res.getColumnIndex(CARD_COLUMN_PIN));
                String area = res.getString(res.getColumnIndex(CARD_COLUMN_AREA));
                double cashlimit = res.getDouble(res.getColumnIndex(CARD_COLUMN_CASHLIMIT));
                double paylimit = res.getDouble(res.getColumnIndex(CARD_COLUMN_PAYLIMIT));
                String account = res.getString(res.getColumnIndex(CARD_COLUMN_ACCOUNT));
                cardList.add(new Bankcard(cardno, pin, cashlimit, paylimit, area, account));
            }
            while (res.moveToNext());
        }
        res.close();
        return cardList;
    }

    public void printCardsFromObjectList() {
        System.out.println("\nCARDS IN THE OBJECT LIST:");
        for (int i=0; i < cardList.size(); i++) {
            System.out.println((i+1)+". Card: "+cardList.get(i).getCardNo() + " Account: "+cardList.get(i).getAccountID()+ " PIN: "+ cardList.get(i).getPinCode()+"\n");
            System.out.println("\tCash limit: "+cardList.get(i).getCashLimit() + " Payment limit: "+cardList.get(i).getPayLimit()+" Area: " +cardList.get(i).getArea() + "\n");
        }
    }

    public void printOneCardObject(String cardNo) {
        for (int i=0; i < cardList.size(); i++) {
            if ((cardList.get(i).getCardNo()).equals(cardNo)) {
                System.out.println("Card: "+cardList.get(i).getCardNo() + " Account: "+cardList.get(i).getAccountID()+ " PIN: "+ cardList.get(i).getPinCode()+"\n");
                System.out.println("\tCash limit: "+cardList.get(i).getCashLimit() + " Payment limit: "+cardList.get(i).getPayLimit()+" Area: " +cardList.get(i).getArea() + "\n");
                break;
            }
        }
    }


    public void deleteCard (String cardNo) {
        SQLiteDatabase db = this.getWritableDatabase();
        int j = 0;
        for (int i=0; i < cardList.size(); i++) {
            if (cardList.get(i).getCardNo().equals(cardNo)) {
                cardList.remove(i);
                db.execSQL("delete from "+ CARD_TABLE_NAME +" where "+ CARD_COLUMN_CARDNO +" = '"+ cardNo +"'");
                System.out.println("*******************\n The bankcard "+ cardNo +" was deleted. \n****************************");
                j = 1;
                break;
            }
        }
        if (j == 0) {
            System.out.println("The bankcard "+cardNo+" doesn't exist.");
        }
        db.close();
    }

    public boolean cardSetArea(String cardNo, String newArea) {
        /* These variables are the original variables of the bankcard object. */
        String oCardNo;
        String oArea;
        boolean passed = false;

        String strSQL;
        SQLiteDatabase db = this.getWritableDatabase();

        /* For loop to go through the object list of all bankcards. The purpose is to find the
         * right object to be able to switch the area of the object, too.*/
        for (int i=0; i < cardList.size(); i++) {
            oCardNo = cardList.get(i).getCardNo();
            oArea = cardList.get(i).getArea();

            if (oCardNo.equals(cardNo)) {
                if (!oArea.equals(newArea)) {
                    strSQL = "UPDATE " + CARD_TABLE_NAME + " SET " + CARD_COLUMN_AREA + " = '" + newArea + "' WHERE " + CARD_COLUMN_CARDNO + " = '" + cardNo + "'";
                    db.execSQL(strSQL); // Update the area in the database
                    cardList.get(i).setArea(newArea); // Update the area in the list of objects
                    System.out.println("The bankcard " + cardNo + " switches the area from " + oArea + " to " + newArea);
                    passed = true;
                }
                else {
                    System.out.println("The area of the bankcard " + cardNo + " is already " + oArea + ".");
                }
            }
        }
        db.close();
        return passed;
    }

    public boolean cardSetCashLimit(String cardNo, double newCashLimit) {
        /* These variables are the original variables of the bankcard object. */
        String oCardNo;
        double oCashLimit;
        boolean passed = false;

        String strSQL;
        SQLiteDatabase db = this.getWritableDatabase();

        /* For loop to go through the object list of all bankcards. The purpose is to find the
         * right object to be able to switch the area of the object, too.*/
        for (int i=0; i < cardList.size(); i++) {
            oCardNo = cardList.get(i).getCardNo();
            oCashLimit = cardList.get(i).getCashLimit();

            if (oCardNo.equals(cardNo)) {
                if (newCashLimit >= 0) {
                    if (oCashLimit != newCashLimit) {
                        strSQL = "UPDATE " + CARD_TABLE_NAME + " SET " + CARD_COLUMN_CASHLIMIT + " = '" + newCashLimit + "' WHERE " + CARD_COLUMN_CARDNO + " = '" + cardNo + "'";
                        db.execSQL(strSQL); // Update the cash limit in the database
                        cardList.get(i).setCashLimit(newCashLimit); // Update the cash limit in the list of objects
                        System.out.println("The bankcard " + cardNo + " switches the cash limit from " + oCashLimit + " to " + newCashLimit);
                        passed = true;
                    } else {
                        System.out.println("The cash limit of the bankcard " + cardNo + " is already " + oCashLimit + ".");
                    }
                }
                else {
                    System.out.println("The cash limit can not be negative.");
                }
            }
        }
        db.close();
        return passed;
    }

    public boolean cardSetPayLimit(String cardNo, double newPayLimit) {
        /* These variables are the original variables of the bankcard object. */
        String oCardNo;
        double oPayLimit;
        boolean passed = false;

        String strSQL;
        SQLiteDatabase db = this.getWritableDatabase();

        /* For loop to go through the object list of all bankcards. The purpose is to find the
         * right object to be able to switch the area of the object, too.*/
        for (int i=0; i < cardList.size(); i++) {
            oCardNo = cardList.get(i).getCardNo();
            oPayLimit = cardList.get(i).getPayLimit();

            if (oCardNo.equals(cardNo)) {
                if (newPayLimit >= 0) {
                    if ((oPayLimit != newPayLimit)) {
                        strSQL = "UPDATE " + CARD_TABLE_NAME + " SET " + CARD_COLUMN_PAYLIMIT + " = '" + newPayLimit + "' WHERE " + CARD_COLUMN_CARDNO + " = '" + cardNo + "'";
                        db.execSQL(strSQL); // Update the cash limit in the database
                        cardList.get(i).setPayLimit(newPayLimit); // Update the cash limit in the list of objects
                        System.out.println("The bankcard " + cardNo + " switches the payment limit from " + oPayLimit + " to " + newPayLimit);
                        passed = true;
                    } else {
                        System.out.println("The payment limit of the bankcard " + cardNo + " is already " + oPayLimit + ".");
                    }
                }
                else {
                    System.out.println("The payment limit can not be negative.");
                }
            }
        }
        db.close();
        return passed;
    }


    public boolean cardSetPinCode(String cardNo, String newPin) {
        /* These variables are the original variables of the bankcard object. */
        String oCardNo;
        String oPin;
        boolean passed = false;

        String strSQL;
        SQLiteDatabase db = this.getWritableDatabase();

        /* For loop to go through the object list of all bankcards. The purpose is to find the
         * right object to be able to switch the PIN code of the object, too.*/
        for (int i=0; i < cardList.size(); i++) {
            oCardNo = cardList.get(i).getCardNo();
            oPin = cardList.get(i).getPinCode();

            if (oCardNo.equals(cardNo)) {
                if (!oPin.equals(newPin)) {
                    if (newPin.trim().length() > 0) {
                        strSQL = "UPDATE " + CARD_TABLE_NAME + " SET " + CARD_COLUMN_PIN + " = '" + newPin + "' WHERE " + CARD_COLUMN_CARDNO + " = '" + cardNo + "'";
                        db.execSQL(strSQL); // Update the area in the database
                        cardList.get(i).setPinCode(newPin); // Update the area in the list of objects
                        System.out.println("The bankcard " + cardNo + " switches the PIN code from " + oPin + " to " + newPin);
                        passed = true;
                    }
                    else {
                        System.out.println("The new PIN-code can not be accepted. Enter a string containing max. 6 digits.");
                        Toast.makeText(usercontext, "PIN-koodissa max 6 numeroa!", Toast.LENGTH_SHORT).show();

                    }
                }
                else {
                    System.out.println("The PIN code of the bankcard " + cardNo + " is already " + oPin + ".");
                }
            }
        }
        db.close();
        return passed;
    }




    /** cardCashWithDrawal is used for taking money out of the bankcard.
     *
     * When proceeding this:
     * * the area of usage,
     * * the correct PIN code and
     * * the limit for taking cash out at once
     * have to be taken into account.
     *
     * That is how this method is different from taking cash out of the bank account through the
     * method takeOrPutCash.
     *
     * The difference in the balance of the bank account is updated in the database and in the
     * object.
     * */
    public ArrayList<Entry> cardCashWithDrawal(String cardNo, String pinCode, String area, double cash, ArrayList<Entry> entryList) {
        Entry entry;
        String method = "Korttinosto";
        /* database variables */
        String strSQL;
        SQLiteDatabase db = this.getWritableDatabase();
        /* bankcard variables */
        String oCardNo;
        String oPinCode;
        String oAccount = "-";
        String oArea;
        double cashLimit;
        /* bankaccount variables */
        double balance;
        double newBalance;
        String type;
        double limit;
        /* The int to check if the cash withdrawal was passed by basic terms */
        int passed = 0;

        /* For loop to go through the object list of all bankcards. The purpose is to find
         * the index of the wanted bankcard and the information about it.
         * When the right index is found, we also check that area, PIN code and amount of cash
         * match the settings of the bankcard.
         * */
        for (int i=0; i < cardList.size(); i++) {
            oCardNo = cardList.get(i).getCardNo();
            if (oCardNo.equals(cardNo)) { // If the right object on the cardList was found:
                oPinCode = cardList.get(i).getPinCode();
                oAccount = cardList.get(i).getAccountID();
                oArea = cardList.get(i).getArea();
                cashLimit = cardList.get(i).getCashLimit();
                if (oArea.equals(area) || oArea.equals("Worldwide") || (oArea.equals("Europe") && area.equals("Finland"))) {
                    if (oPinCode.equals(pinCode)) {
                        if (cash <= cashLimit) {
                            passed = 1;
                            System.out.println("The area, PIN code and the amount of cash OK, passed.");
                        } else {
                            System.out.println("The amount of cash exceeds the cash limit set on this bankcard.");
                            Toast.makeText(usercontext, "Nostoraja ylittyi", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        System.out.println("The entered PIN code was incorrect.");
                        Toast.makeText(usercontext, "PIN väärin!", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    System.out.println("The bankcard is not usable in this geographical area.");
                    Toast.makeText(usercontext, "Kortti ei toimi tällä alueella", Toast.LENGTH_SHORT).show();

                }
                break;
            }
        }
        if (oAccount.equals("-")) {
            System.out.println("Error trying to find the bank account linked to this bankcard. Make sure the cardno is correct.");
        }
        if (passed == 1) { // The passed == 0 if the area, pin code or amount of cash do not pass.
            for (int i = 0; i < accountList.size(); i++) {
                if ((accountList.get(i).getAccountID()).equals(oAccount)) { // If the right object on the accountList was found:
                    balance = accountList.get(i).getBalance();
                    type = accountList.get(i).getType();
                    limit = accountList.get(i).getLimit();
                    newBalance = balance - cash; // cash has to be positive!

                    if (type.equals("Debit")) {
                        if (balance >= (cash)) {
                            strSQL = "UPDATE " + DEBIT_TABLE_NAME + " SET " + DEBIT_COLUMN_BALANCE + " = " + newBalance + " WHERE " + DEBIT_COLUMN_ACCOUNT + " = '" + oAccount + "'";
                            db.execSQL(strSQL);
                            accountList.get(i).setBalance(-cash);

                            /** Create an entry object */
                            Date dateTime = new Date();
                            entry = new Entry(dateTime, oAccount, "-", method, cash);
                            entryList.add(entry);

                            System.out.println("The new balance of the account "+oAccount+" linked to this bankcard is " + newBalance);
                            Toast.makeText(usercontext, "Nosto onnistui", Toast.LENGTH_SHORT).show();
                        } else {
                            System.out.println("The balance of the account linked to this bankcard is " + balance + ". It's not possible to proceed the withdrawal.");
                            Toast.makeText(usercontext, "Nosto epäonnistui, tarkista saldo", Toast.LENGTH_SHORT).show();
                        }
                    } else if (type.equals("Credit")) {
                        if ((balance - cash) >= (-limit)) {
                            strSQL = "UPDATE " + CREDIT_TABLE_NAME + " SET " + CREDIT_COLUMN_BALANCE + " = " + newBalance + " WHERE " + CREDIT_COLUMN_ACCOUNT + " = '" + oAccount + "'";
                            db.execSQL(strSQL);
                            accountList.get(i).setBalance(-cash);

                            /** Create an entry object */
                            Date dateTime = new Date();
                            entry = new Entry(dateTime, oAccount, "-", method, cash);
                            entryList.add(entry);

                            System.out.println("The new balance of the account "+oAccount+" linked to this bankcard is " + newBalance);
                            Toast.makeText(usercontext, "Nosto onnistui", Toast.LENGTH_SHORT).show();
                        } else {
                            System.out.println("The credit limit of the account linked to this bankcard is " + limit + ". It's not possible to proceed the withdrawal.");
                            Toast.makeText(usercontext, "Nosto epäonnistui, tarkista saldo", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
        db.close();
        return entryList;
    }

    /** cardPayment is used for paying on the bankcard instead of the cash.
     *
     * When proceeding this:
     * * the area of usage,
     * * the correct PIN code and
     * * the maximum sum limit for making a bankcard payment
     * have to be taken into account.
     *
     * The difference in the balance of the bank account is updated in the database and in the
     * object.
     * */
    public ArrayList<Entry> cardPayment(String cardNo, String pinCode, String area, double sum,ArrayList<Entry> entryList) {
        Entry entry = null;
        String method = "Korttimaksu";
        /* database variables */
        String strSQL;
        SQLiteDatabase db = this.getWritableDatabase();
        /* bankcard variables */
        String oCardNo;
        String oPinCode;
        String oAccount = "-";
        String oArea;
        double payLimit;
        /* bankaccount variables */
        double balance;
        double newBalance;
        String type;
        double limit;
        /* The int to check if the cash withdrawal was passed by basic terms */
        int passed = 0;

        /* For loop to go through the object list of all bankcards. The purpose is to find
         * the index of the wanted bankcard and the information about it.
         * When the right index is found, we also check that area, PIN code and amount of cash
         * match the settings of the bankcard.
         * */
        for (int i=0; i < cardList.size(); i++) {
            oCardNo = cardList.get(i).getCardNo();
            if (oCardNo.equals(cardNo)) { // If the right object on the cardList was found:
                oPinCode = cardList.get(i).getPinCode();
                oAccount = cardList.get(i).getAccountID();
                oArea = cardList.get(i).getArea();
                payLimit = cardList.get(i).getPayLimit();
                if (oArea.equals(area) || oArea.equals("Worldwide") || (oArea.equals("Europe") && area.equals("Finland"))) {
                    if (oPinCode.equals(pinCode)) {
                        if (sum <= payLimit) {
                            passed = 1;
                            System.out.println("The area, PIN code and the amount of cash OK, passed.");
                        } else {
                            System.out.println("The amount of cash exceeds the cash limit set on this bankcard.");
                            Toast.makeText(usercontext, "Maksuraja ylittyi", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        System.out.println("The entered PIN code was incorrect.");
                        Toast.makeText(usercontext, "PIN väärin", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    System.out.println("The bankcard is not usable in this geographical area.");
                    Toast.makeText(usercontext, "Kortti ei toimi tällä alueella", Toast.LENGTH_SHORT).show();

                }
                break;
            }
        }

        if (oAccount.equals("-")) {
            System.out.println("Error trying to find the bank account linked to this bankcard. Make sure the cardno is correct.");
        }

        if (passed == 1) { // The passed == 0 if the area, pin code or amount of cash do not pass.
            for (int i = 0; i < accountList.size(); i++) {
                if ((accountList.get(i).getAccountID()).equals(oAccount)) { // If the right object on the accountList was found:
                    balance = accountList.get(i).getBalance();
                    type = accountList.get(i).getType();
                    limit = accountList.get(i).getLimit();
                    newBalance = balance - sum; // sum has to be positive!

                    if (type.equals("Debit")) {
                        if (balance >= (sum)) {
                            strSQL = "UPDATE " + DEBIT_TABLE_NAME + " SET " + DEBIT_COLUMN_BALANCE + " = " + newBalance + " WHERE " + DEBIT_COLUMN_ACCOUNT + " = '" + oAccount + "'";
                            db.execSQL(strSQL);
                            accountList.get(i).setBalance(-sum);

                            /** Create an entry object */
                            Date dateTime = new Date();
                            entry = new Entry(dateTime, oAccount, "-", method, sum);
                            entryList.add(entry);

                            System.out.println("The new balance of the account "+oAccount+" linked to this bankcard is " + newBalance);
                        } else {
                            System.out.println("The balance of the account linked to this bankcard is " + balance + ". It's not possible to proceed the card payment.");
                        }
                    } else if (type.equals("Credit")) {
                        if ((balance - sum) >= (-limit)) {
                            strSQL = "UPDATE " + CREDIT_TABLE_NAME + " SET " + CREDIT_COLUMN_BALANCE + " = " + newBalance + " WHERE " + CREDIT_COLUMN_ACCOUNT + " = '" + oAccount + "'";
                            db.execSQL(strSQL);
                            accountList.get(i).setBalance(-sum);

                            /** Create an entry object */
                            Date dateTime = new Date();
                            entry = new Entry(dateTime, oAccount, "-", method, sum);
                            entryList.add(entry);

                            System.out.println("The new balance of the account "+oAccount+" linked to this bankcard is " + newBalance);
                            Toast.makeText(usercontext, "Maksu onnistui", Toast.LENGTH_SHORT).show();
                        } else {
                            System.out.println("The credit limit of the account linked to this bankcard is " + limit + ". It's not possible to proceed the card payment.");
                            Toast.makeText(usercontext, "Maksu epäonnistui, tarkista saldo", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
        db.close();
        return entryList;
    }

    /************** CLASS METHODS ENDED ******************/

    @Override
    public synchronized void close() {
        if (mDataBase != null)
            mDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion)
            mNeedUpdate = true;
    }

    public void emptyLists() {
        userList.clear();
        accountList.clear();

        System.out.println("UserList and accountList have been cleared");
    }



}