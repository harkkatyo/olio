package com.example.harkkatyo;

import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Bank{
    DatabaseHelper mDBHelper;
    SQLiteDatabase mDataBase;
    private String name;
    ArrayList<User> userList;
    ArrayList<Account> accountList;
    ArrayList<Bankcard> cardList;
    ArrayList<Entry> entryList;
    CreateEntryXML entryXML;
    Context mContext;

    private static Bank B = new Bank("OmaPankki");

    private Bank(String bankname) {
        name = bankname;
    }

    public static Bank getBank() {
        return B;
    }

    public String getBankname(){
        return name;
    }

    /*Opens database and reads it to the lists*/

    protected void openDatabase(Context context) {
        mContext = context;

        mDBHelper = new DatabaseHelper(context);

        try {
            mDBHelper.updateDataBase();
        } catch (IOException mIOException) {
            throw new Error("UnableToUpdateDatabase");
        }

        try {
            mDataBase = mDBHelper.getWritableDatabase();
        } catch (SQLException mSQLException) {
            throw mSQLException;
        }

        userList = mDBHelper.putAllUsersInTheList();
        accountList = mDBHelper.putAllAccountsInTheList();
        cardList = mDBHelper.putAllCardsInTheList();


        mDBHelper.printAccountsFromObjectList();
        mDBHelper.printUsersFromObjectList();

    }

        /*Creates random number to account and adds type in front of it. After that creates account.*/
    public boolean createAccount(String id, String type, double interest, double limit) {
        int random = new Random().nextInt(100000000) + 10000000;
        String account = Integer.toString(random);

        if (type.equals("Debit")) {
            account = "DE" + account;
        } else if(type.equals("Saving")) {
            account = "SA" + account;
        } else {
            account = "CR" + account;
        }
        mDBHelper.insertAccount(id,type, account, 0, interest, limit);
        return true;
    }

    public boolean createBankcard(String area, String nosto, String maksu, String account, String pin) {
        boolean passed = false;
        int random = new Random().nextInt(1000000) + 100000;
        String cardID = Integer.toString(random);
        cardID = "CARD" + cardID;
        double cashlimit = Double.parseDouble(nosto);
        double paylimit = Double.parseDouble(maksu);
        passed = mDBHelper.insertBankCard(cardID,pin,cashlimit, paylimit,area,account);
        return passed;
    }

    /*METHODS TO UPDATE XML-FILE */

    public void updateXMLFile() {
        entryXML.writeXMLFile(entryList);
        entryXML.printXMLFile();

    }

    public void takeOrPut(String account, double cash) {
        entryList = mDBHelper.takeOrPutCash(account, cash, entryList);
        updateXMLFile();
    }

    public void transferMoney(String sendAccount, String getAccount, double sum) {
        entryList = mDBHelper.transferMoney(sendAccount, getAccount, sum, entryList);
        updateXMLFile();
    }

    public void takeCash(String cardNo, String pinCode, String area, double cash){
        entryList = mDBHelper.cardCashWithDrawal(cardNo, pinCode, area, cash, entryList);
        updateXMLFile();
    }

    public void cardPayment(String cardNo, String pinCode, String area, double sum){
        entryList = mDBHelper.cardPayment(cardNo, pinCode, area, sum, entryList);
        updateXMLFile();
    }


    /*Method to read transactions from list (created from XML)*/
    public ArrayList getTransactions(String account){
        ArrayList actions = new ArrayList();
        for (int i = 0; i < entryList.size(); i++) {
            if (entryList.get(i).getSendAccount().equals(account)  || entryList.get(i).getGetAccount().equals(account)) {
                String dateTime = entryList.get(i).getDateTime();
                String sendAccount = entryList.get(i).getSendAccount();
                String getAccount = entryList.get(i).getGetAccount();
                String method = entryList.get(i).getMethod();
                String sum = entryList.get(i).getSum();
                actions.add(dateTime + " "+method+ " \nLähettäjä: " + sendAccount + " Saaja: " + getAccount + "\n " + " Summa: " + sum);
            }
        }


        return actions;
    }

    public void readXML(){
        entryList = new ArrayList<>();
        entryXML = new CreateEntryXML(entryList, mContext);
        entryXML.readEntriesToObjectList(entryList);
        printEntry();
    }

    public void printEntry(){
        for (int i = 0; i < entryList.size(); i++) {
            String dateTime = entryList.get(i).getDateTime();
            String sendAccount = entryList.get(i).getSendAccount();
            String getAccount = entryList.get(i).getGetAccount();
            String method = entryList.get(i).getMethod();
            String sum = entryList.get(i).getSum();
            System.out.println(dateTime + " "+method+ " \nLähettäjä: " + sendAccount + " Saaja: " + getAccount + "\n " + " Summa: " + sum);
        }
    }

    public boolean insertUser(String id, String salasana, String bank, String fn, String ln, String ph,String em){
        boolean passed = mDBHelper.insertUser(id, salasana, bank, fn, ln, ph, em, userList);
        return passed;
    }

}


