package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class fragment_editContacts extends Fragment {
    EditText newEmail;
    EditText newPhone;
    EditText newPassword;
    EditText checkPassword;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_editcontacts, container, false);
        newEmail = view.findViewById(R.id.newEmail);
        newPhone = view.findViewById(R.id.newPhone);
        newPassword = view.findViewById(R.id.newPassword);
        checkPassword = view.findViewById(R.id.repeatPassword);
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


}
