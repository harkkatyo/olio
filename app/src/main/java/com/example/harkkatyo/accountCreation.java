package com.example.harkkatyo;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class accountCreation extends AppCompatActivity {
    Spinner spinner;
    List<String> list;
    Fragment fragment = null;
    String choice;
    String id;
    Bank pankki = Bank.getBank();
    Boolean passed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_creation);
        if (getIntent().hasExtra("kayttaja")) {
            id = getIntent().getStringExtra("kayttaja");
        }
        addItemsOnSpinner();
    }

    public void addItemsOnSpinner(){
        spinner = (Spinner) findViewById(R.id.spinner);
        list = new ArrayList<>();
        list.add("Valitse tilityyppi:");
        list.add("Debit");
        list.add("Credit");
        list.add("Saving");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                choice = list.get(position);
                if (choice.equals("Debit"));
                else if (position != 0) {
                    if (choice.equals("Credit")) {
                        fragment = new fragment_createCredit();
                    } else if (choice.equals("Saving")) {
                        fragment = new fragment_createSaving();
                    } else {
                        fragment = null;
                    }
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.window, fragment);
                    ft.commit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        Button button = (Button) findViewById(R.id.accept);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int interestID;
                double interest = 0;
                double limit = 0;
                FragmentManager manager = getSupportFragmentManager();
                Intent intent;
                if (getIntent().hasExtra("from")) {
                    intent = new Intent(accountCreation.this, adminApplication.class);
                } else {
                    intent = new Intent(accountCreation.this, userApplication.class);

                }
                if (!choice.equals("\"Valitse tilityyppi:")) {
                    if (choice.equals("Credit")) {
                        fragment_createCredit credit_obj = (fragment_createCredit) manager.findFragmentById(R.id.window);
                        String inputFromFragment = credit_obj.limit.getText().toString();
                        if (!inputFromFragment.equals("")) {
                            double inputlimit = Double.parseDouble(inputFromFragment);
                            if (inputlimit>0) {
                                limit = inputlimit;
                                passed = pankki.createAccount(id, choice, interest, limit );
                            }
                        }
                    } else if (choice.equals("Saving")) {
                        fragment_createSaving saving_obj = (fragment_createSaving) manager.findFragmentById(R.id.window);
                        interestID = saving_obj.rg.getCheckedRadioButtonId();
                        RadioButton radioSexButton = (RadioButton) findViewById(interestID);
                        if (interestID == -1) {
                            Toast.makeText(accountCreation.this,
                                    "Anna ensin korko!", Toast.LENGTH_SHORT).show();
                        } else {

                            if (radioSexButton.getText().toString().equals("Korkea")) {
                                interest = 2.5;
                            } else if (radioSexButton.getText().toString().equals("Keski")) {
                                interest = 1.25;
                            } else if (radioSexButton.getText().toString().equals("Matala")) {
                                interest = 0.5;
                            }
                            if (interest != 0) {
                                passed = pankki.createAccount(id, choice, interest, limit);
                            }
                        }
                    } else if (choice.equals("Debit")) {
                        passed = pankki.createAccount(id, choice, interest, limit );
                    }

                    if (passed) {
                        intent.putExtra("kayttaja", id);
                        intent.putExtra("message", "Tili luotu onnistuneesti");
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(accountCreation.this,
                                "Tilin luominen epäonnistui!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }



}
