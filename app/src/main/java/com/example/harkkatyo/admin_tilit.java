package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class admin_tilit extends Fragment {
    Spinner spinner;
    TextView text;
    Bank pankki = Bank.getBank();
    Button buttonCreate;
    Button buttonDelete;
    String userID;
    Spinner spinnerAC;
    List acc;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.admin_tilit, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner = (Spinner) view.findViewById(R.id.SpinnerKäyttäjä);
        text = (TextView) view.findViewById(R.id.textView1200);
        buttonCreate = (Button) view.findViewById(R.id.button7);
        buttonDelete = (Button) view.findViewById(R.id.button8);
        spinnerAC = (Spinner) view.findViewById(R.id.spinner5);
        acc = new ArrayList();
        text.setMovementMethod(new ScrollingMovementMethod());
        setItemsOnSpinner();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userID = spinner.getSelectedItem().toString();
                setItemsOnSpinner2();
                getAccounts(userID);
                if (!userID.equals("Valitse käyttäjä:")) {
                    buttonDelete.setVisibility(View.VISIBLE);
                    buttonCreate.setVisibility(View.VISIBLE);
                    spinnerAC.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ac = spinnerAC.getSelectedItem().toString();
                if (!ac.equals("Valitse poistettava tili:")){
                    pankki.mDBHelper.deleteAccount(ac);
                    getAccounts(userID);
                    Toast.makeText(getActivity(), "Tili poistettu", Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), accountCreation.class);
                intent.putExtra("kayttaja", userID);
                intent.putExtra("from", "admin");
                startActivity(intent);
            }
        });

    }

    /*Users to spinner*/
    public void setItemsOnSpinner(){
        List users = new ArrayList();
        users.add("Valitse käyttäjä:");
        for (int i = 0; i < pankki.userList.size(); i++) {
            users.add(pankki.userList.get(i).getUserID());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, users);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /*Display all accounts*/
    public void getAccounts(String userID) {
        text.setText(null);
        DecimalFormat df = new DecimalFormat("0.00");
        int numb = 0;
        for (int i = 0; i < pankki.accountList.size(); i++) {
            if (userID.equals(pankki.accountList.get(i).getUserID())){
                numb = 1;
                String newBalance = df.format(pankki.accountList.get(i).getBalance());
                text.setText(text.getText().toString() +"\n"+ pankki.accountList.get(i).getType()+ " " +pankki.accountList.get(i).getAccountID() +"\n" );
                text.setText(text.getText().toString()+ "Saldo: "+ newBalance + "\n");
                if (pankki.accountList.get(i).getType().equals("Credit")) {
                    text.setText(text.getText().toString()+ " Luottoraja: " + pankki.accountList.get(i).getLimit() + "\n");
                } else if (pankki.accountList.get(i).getType().equals("Saving")) {
                    String newInterest = df.format(pankki.accountList.get(i).getInterest());
                    text.setText(text.getText().toString()+ " Korko: " + newInterest + "\n");
                }
            }
        }
        if (numb == 0 && !userID.equals("Valitse käyttäjä:")) {
            text.setText("Ei tilejä");
        }
    }

    /*All accounts to spinner*/
    private void setItemsOnSpinner2(){
        acc.clear();
        acc.add("Valitse poistettava tili:");
        for (int i = 0; i < pankki.accountList.size(); i++) {
            if (userID.equals(pankki.accountList.get(i).getUserID())) {
                acc.add(pankki.accountList.get(i).getAccountID());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, acc);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAC.setAdapter(adapter);
    }



}
