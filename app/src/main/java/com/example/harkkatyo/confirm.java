package com.example.harkkatyo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class confirm extends AppCompatActivity {
    TextView koodi;
    EditText input;
    TextView mistake;
    int random;
    String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        koodi = (TextView) findViewById(R.id.code);
        input = (EditText) findViewById(R.id.inputcode);
        mistake = (TextView) findViewById(R.id.mistake);
        random = new Random().nextInt(1000000) + 100000;
        koodi.setText(Integer.toString(random));
        if (getIntent().hasExtra("kayttaja")) {
            user = getIntent().getStringExtra("kayttaja");
        }
    }

    /*Checks if the input is the same as given 6 number code. If is, moves to application*/

    public void onClick(View v) {
        String numberIn = input.getText().toString();
        if (!numberIn.equals("")) {
            int number = Integer.parseInt(numberIn);
            if (number == random) {
                Intent intent = new Intent(confirm.this, userApplication.class);
                intent.putExtra("kayttaja", user);
                startActivity(intent);
                finish();
            }
            else {
                mistake.setText("Virheellinen numerosarja!");
            }
        }else {
            mistake.setText("Tyhjä syöte!");
        }
    }
}
