package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class fragment_editCard extends Fragment {
    EditText newDraw;
    EditText newPay;
    Spinner spinner;
    EditText newPin;
    EditText checkPin;
    TextView text;
    List list3;
    Bank pankki = Bank.getBank();
    String userid;
    String cardid;
    CheckBox box;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        userid = getArguments().getString("kayttajaID");
        cardid = getArguments().getString("cardID");
        return inflater.inflate(R.layout.layout_editcard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        newDraw = (EditText) view.findViewById(R.id.nostorajaUusi);
        newPay = (EditText) view.findViewById(R.id.maksurajaUusi);
        newPin = (EditText) view.findViewById(R.id.pinUusi);
        checkPin = (EditText) view.findViewById(R.id.pinVahvista);
        text = (TextView) view.findViewById(R.id.näytäTiedot);
        spinner = (Spinner) view.findViewById(R.id.areaUusi);
        text.setMovementMethod(new ScrollingMovementMethod());
        box = (CheckBox) view.findViewById(R.id.checkBox);
        setItemsOnSpinner();
        setText();
    }

    public void setItemsOnSpinner(){
        list3 = new ArrayList<>();
        list3.add("Valitse uusi alue:");
        list3.add("Finland");
        list3.add("Europe");
        list3.add("Asia");
        list3.add("North-America");
        list3.add("South-America");
        list3.add("Africa");
        list3.add("Oceania");
        list3.add("Worldwide");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list3);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /*Display card information*/
    public void setText(){
        text.setText(null);
        for (int i=0; i < pankki.accountList.size(); i++) {
            if (userid.equals(pankki.accountList.get(i).getUserID())) {
                String accountid = pankki.accountList.get(i).getAccountID();
                for (int a=0; a < pankki.cardList.size();a++) {
                    if (accountid.equals(pankki.cardList.get(a).getAccountID()) && cardid.equals(pankki.cardList.get(a).getCardNo())) {
                        text.setText(text.getText().toString() + pankki.cardList.get(a).getCardNo() + " pin: " + pankki.cardList.get(a).getPinCode() + "\n");
                        text.setText(text.getText().toString() + "Nostoraja: " + pankki.cardList.get(a).getCashLimit() + "  Maksuraja: " + pankki.cardList.get(a).getPayLimit() + "\n" );
                        text.setText(text.getText().toString() + "Toimivuusalue: " + pankki.cardList.get(a).getArea() + "\n");
                        text.setText(text.getText().toString() + "Linkitetty tili: " + pankki.cardList.get(a).getAccountID());
                    }
                }
            }
        }
    }
}
