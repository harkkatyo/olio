package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class fragment_tilisiirto extends Fragment {
    EditText firstname;
    EditText lastname;
    EditText amount;
    List<String> ownAccounts;
    List<String> otherAccounts;
    Bank pankki = Bank.getBank();
    String id;
    EditText inputaccount;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View view = inflater.inflate(R.layout.layout_tilisiirto, container, false);
        id = getArguments().getString("kayttajaID");
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Siirrä rahaa tililtä");

        final Spinner ownAccountSpinner;
        final Spinner secondSpinner;

        Button buttonFind = (Button) view.findViewById(R.id.findAccounts);
        Button buttonDo = (Button) view.findViewById(R.id.acceptTransfer);
        firstname = (EditText) view.findViewById(R.id.firstN);
        lastname = (EditText) view.findViewById(R.id.lastN);
        amount = (EditText) view.findViewById(R.id.transferredAmount);
        ownAccountSpinner = (Spinner) view.findViewById(R.id.spinner3);
        secondSpinner = (Spinner) view.findViewById(R.id.spinner4);
        inputaccount = (EditText) view.findViewById(R.id.editText4123);
        ownAccounts = new ArrayList<>();
        otherAccounts = new ArrayList<>();

        /*Set own accounts to spinner*/

        ownAccounts.add("Valitse tili:");
        for (int i = 0; i < pankki.accountList.size(); i++) {
            if (id.equals(pankki.accountList.get(i).getUserID())){
                if (!pankki.accountList.get(i).getType().equals("Saving")) {
                    ownAccounts.add(pankki.accountList.get(i).getAccountID());
                }
            }
        }

        addItemsOnSpinner1(ownAccountSpinner, ownAccounts);


        buttonFind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fName = firstname.getText().toString();
                String lName = lastname.getText().toString();

                /*If user hasn't given name to find, get users first and last name*/
                if (fName.equals("") && lName.equals("")) {
                    for (int i=0; i < pankki.userList.size(); i++) {
                        if (pankki.userList.get(i).getUserID().equals(id)) {
                            fName = pankki.userList.get(i).getFirstName();
                            lName = pankki.userList.get(i).getLastName();
                        }
                    }

                }

                /*Get accounts based on the name*/
                for (int i = 0; i < pankki.userList.size(); i++) {
                    if (fName.equals(pankki.userList.get(i).getFirstName()) && lName.equals(pankki.userList.get(i).getLastName())){
                        String otherId = pankki.userList.get(i).getUserID();
                        for (int a = 0; a < pankki.accountList.size(); a++) {
                            if (otherId.equals(pankki.accountList.get(a).getUserID())) {
                                /*Sender and receiver accounts can't be same*/
                                if (!pankki.accountList.get(i).getAccountID().equals(ownAccountSpinner.getSelectedItem().toString())) {
                                    otherAccounts.add(pankki.accountList.get(a).getAccountID());
                                }
                            }
                        }
                    }
                }
                if (otherAccounts.isEmpty()) {
                    Toast.makeText(getActivity(), "Käyttäjää tai tilejä ei löydy", Toast.LENGTH_SHORT).show();
                } else {
                    addItemsOnSpinner1(secondSpinner, otherAccounts);
                }
            }
        });

        buttonDo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sender = ownAccountSpinner.getSelectedItem().toString();
                String receiver = inputaccount.getText().toString();
                /*If input account is empty, take it from spinner*/
                if (receiver.equals("")) {
                    try {
                        receiver = secondSpinner.getSelectedItem().toString();
                    }catch (NullPointerException ex) {
                        Toast.makeText(getActivity(), "Valitse tili!", Toast.LENGTH_SHORT).show();
                    }
                }
                if (!amount.getText().toString().equals("")) {
                    double moneyAmount = Double.parseDouble(amount.getText().toString());
                    pankki.transferMoney(sender, receiver, moneyAmount);
                } else {
                    Toast.makeText(getActivity(),"Summa puuttuu!", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    public void addItemsOnSpinner1(Spinner spinner, List list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

}
