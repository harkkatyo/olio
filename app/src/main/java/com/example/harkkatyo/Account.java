package com.example.harkkatyo;


public class Account {
    String userID;
    String accountID;
    String type;
    double balance;
    double interest;
    double limit;

    public Account(String new_userID, String new_accountID, String new_type, double new_balance, double new_interest, double new_limit) {
        userID = new_userID;
        accountID = new_accountID;
        type = new_type; // Credit, Debit, Saving
        balance = new_balance;
        interest = new_interest;
        limit = new_limit;
    }
    public String getUserID() { return userID;}
    public String getAccountID() { return accountID;}
    public String getType() { return type;}
    public double getBalance() { return balance;}
    public double getInterest() { return interest;}
    public double getLimit() { return limit;}
    /******************* ACCOUNT-CLASSISSA: *******************/

    public void setBalance(double cash) { this.balance += cash; }
}

