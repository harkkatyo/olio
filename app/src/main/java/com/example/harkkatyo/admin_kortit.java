package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class admin_kortit extends Fragment {
    Spinner spinner;
    TextView text;
    Bank pankki = Bank.getBank();
    Spinner cardSPinner;
    List cardList;
    String userID;
    Button buttonNew;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.admin_kortit, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner = (Spinner) view.findViewById(R.id.SpinnerKäyttäjä);
        text = (TextView) view.findViewById(R.id.textView1200);
        cardSPinner = (Spinner) view.findViewById(R.id.spinner5cards);
        text.setMovementMethod(new ScrollingMovementMethod());

        setItemsOnSpinner();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userID = spinner.getSelectedItem().toString();
                setText(userID);
                setItemsOnSpinner2();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        buttonNew = (Button) view.findViewById(R.id.button10);
        userID = spinner.getSelectedItem().toString();

        buttonNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!userID.equals("")) {
                    Intent intent = new Intent(getActivity(), BankcardCreation.class);
                    intent.putExtra("kayttaja", userID);
                    intent.putExtra("from", "kortit");
                    startActivity(intent);
                }
            }
        });

        Button button = (Button) view.findViewById(R.id.button5);
        final Button buttonUpdate = (Button) view.findViewById(R.id.button6);
        /*Open fragment when wanted to change card settings*/
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonUpdate.setVisibility(View.VISIBLE);
                Fragment fragment = new admin_editCard();
                FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                ft.replace(R.id.admin_editcardF, fragment);
                ft.commit();
            }
        });
        /*Update card and display new information*/
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCard();
                setText(userID);
                setItemsOnSpinner2();

            }
        });
    }
    /*Add users to spinner*/
    public void setItemsOnSpinner(){
        List users = new ArrayList();
        users.add("Valitse käyttäjä:");
        for (int i = 0; i < pankki.userList.size(); i++) {
            users.add(pankki.userList.get(i).getUserID());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, users);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /*Display all cards from wanted user and add them to list*/
    private void setText(String userID) {
        cardList = new ArrayList();
        text.setText(null);
        for (int i=0; i < pankki.accountList.size(); i++) {
            if (userID.equals(pankki.accountList.get(i).getUserID())) {
                String accountid = pankki.accountList.get(i).getAccountID();
                for (int a=0; a < pankki.cardList.size();a++) {
                    if (accountid.equals(pankki.cardList.get(a).getAccountID())) {
                        text.setText(text.getText().toString() +"\n\n"+ pankki.cardList.get(a).getCardNo() + " PIN: " + pankki.cardList.get(a).getPinCode() + " Alue: "+pankki.cardList.get(a).getArea()+"\n");
                        text.setText(text.getText().toString() + "Nostoraja: " + pankki.cardList.get(a).getCashLimit() + " Maksuraja: " + pankki.cardList.get(a).getPayLimit());
                        cardList.add(pankki.cardList.get(a).getCardNo());
                    }
                }
            }
        }
        if (cardList.isEmpty() && !userID.equals("Valitse käyttäjä:")){
            text.setText("Ei kortteja");
        }
    }

    /*Add cards to spinner*/
    private void setItemsOnSpinner2(){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, cardList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cardSPinner.setAdapter(adapter);
    }

    /*Check changed information and update card. Delete card if box is checked*/
    private void updateCard(){
        admin_editCard edit_obj = (admin_editCard) new admin_editCard();
        String card = cardSPinner.getSelectedItem().toString();
        String newArea = edit_obj.spinner2.getSelectedItem().toString();
        String newDraw = edit_obj.newDraw.getText().toString();
        String newPay = edit_obj.newPay.getText().toString();
        String newPIN = edit_obj.newPin.getText().toString();
        String checkPIN = edit_obj.checkPin.getText().toString();
        CheckBox box = edit_obj.box;
        boolean passed = false;

        if (!newArea.equals("Valitse uusi alue:")) {
            passed = pankki.mDBHelper.cardSetArea(card,newArea);
        }
        if (!newDraw.equals("")) {
            double newDrawIn = Double.parseDouble(newDraw);
            passed = pankki.mDBHelper.cardSetCashLimit(card, newDrawIn);
        }
        if (!newPay.equals("")) {
            double newPayIn = Double.parseDouble(newPay);
            passed = pankki.mDBHelper.cardSetPayLimit(card, newPayIn);
        }
        if (!newPIN.equals("") && !checkPIN.equals("") && newPIN.equals(checkPIN)) {
            passed =  pankki.mDBHelper.cardSetPinCode(card, newPIN);
        }
        if (box.isChecked()) {
            pankki.mDBHelper.deleteCard(card);
            Toast.makeText(getContext(), "Kortti poistettu", Toast.LENGTH_SHORT).show();
        }
        if (passed){
            Toast.makeText(getContext(),"Tiedot päivitetty", Toast.LENGTH_SHORT).show();
        }


    }

}
