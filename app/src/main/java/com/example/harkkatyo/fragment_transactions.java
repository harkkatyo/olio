package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class fragment_transactions extends Fragment {
    Spinner spinner;
    TextView text;
    String userid;
    Bank pankki = Bank.getBank();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        userid = getArguments().getString("kayttajaID");
        return inflater.inflate(R.layout.layout_transactions, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner = (Spinner) view.findViewById(R.id.tiliSpinner);
        text = (TextView) view.findViewById(R.id.textView1200);
        text.setMovementMethod(new ScrollingMovementMethod());
        setItemsOnSpinner();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (!spinner.getSelectedItem().toString().equals("Valitse tarkasteltava tili:")){
                    ArrayList actions = pankki.getTransactions(spinner.getSelectedItem().toString());
                    printActions(actions);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    public void setItemsOnSpinner(){
        List accounts = new ArrayList();
        accounts.add("Valitse tarkasteltava tili:");
        for (int i = 0; i < pankki.accountList.size(); i++) {
            if (userid.equals(pankki.accountList.get(i).getUserID())){
                    accounts.add(pankki.accountList.get(i).getAccountID());
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, accounts);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public void printActions(ArrayList list) {
        text.setText(null);
        if (list.isEmpty()) {
            text.setText("Ei tilitapahtumia");
        } else {
            for (int i = 0; i < list.size(); i++) {
                text.setText(text.getText().toString() + "\n" + list.get(i) + "\n\n");
            }
        }
    }

}
