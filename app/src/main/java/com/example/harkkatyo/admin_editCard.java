package com.example.harkkatyo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class admin_editCard extends Fragment {
    Spinner spinner;
    Spinner spinner2;
    Bank pankki = Bank.getBank();
    EditText newDraw;
    EditText newPay;
    EditText newPin;
    EditText checkPin;
    CheckBox box;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.admin_editcard, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner = (Spinner) view.findViewById(R.id.SpinnerKäyttäjä);
        newDraw = (EditText) view.findViewById(R.id.nostorajaUusi);
        newPay = (EditText) view.findViewById(R.id.maksurajaUusi);
        newPin = (EditText) view.findViewById(R.id.pinUusi);
        checkPin = (EditText) view.findViewById(R.id.pinVahvista);
        spinner2 = (Spinner) view.findViewById(R.id.areaUusi);
        box = (CheckBox) view.findViewById(R.id.checkBox);
        setItemsOnSpinner();

    }

    public void setItemsOnSpinner(){
        List list3 = new ArrayList<>();
        list3.add("Valitse uusi alue:");
        list3.add("Finland");
        list3.add("Europe");
        list3.add("Asia");
        list3.add("North-America");
        list3.add("South-America");
        list3.add("Africa");
        list3.add("Oceania");
        list3.add("Worldwide");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list3);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }
}
