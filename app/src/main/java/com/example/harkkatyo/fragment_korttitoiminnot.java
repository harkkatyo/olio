package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class fragment_korttitoiminnot extends Fragment {
    Spinner spinner;
    List list;
    Fragment fragment = null;
    String userid;
    Spinner spinner2;
    Bank pankki = Bank.getBank();
    List list2;
    Button button;
    String choice;
    String card;
    EditText pinni;
    String cardPIN;
    boolean passed;
    Button buttonHae;
    String message;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        userid = getArguments().getString("kayttajaID");
        return inflater.inflate(R.layout.layout_korttitoiminnot, container, false);
    }


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner = (Spinner) view.findViewById(R.id.spinner0020);
        spinner2 = (Spinner) view.findViewById(R.id.spinnerCard);
        button = (Button) view.findViewById(R.id.buttonnro100);
        pinni = (EditText) view.findViewById(R.id.editText400);
        buttonHae = (Button) view.findViewById(R.id.btn500);

        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Korttitoiminnot");
        setItemsOnSpinner();
        setItemsOnSpinner2();

        buttonHae.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choice = spinner.getSelectedItem().toString();
                card = spinner2.getSelectedItem().toString();
                findPIN();
                if (choice.equals("Valitse toiminto"));
                else {
                    if (choice.equals("Nosta kortilta")) {
                        fragment = new fragment_drawCard();
                        pinni.setHint("Syötä PIN");
                    } else if (choice.equals("Maksa kortilla")) {
                        fragment = new fragment_payCard();
                        pinni.setHint("Syötä PIN");
                    } else if (choice.equals("Näytä kortin tiedot")) {
                        fragment = new fragment_editCard();
                        pinni.setHint("Syötä vanha PIN");
                    }
                    FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("kayttajaID",userid);
                    bundle.putString("cardID", card);
                    fragment.setArguments(bundle);
                    ft.replace(R.id.cardFragment, fragment);
                    ft.commit();
                    pinni.setVisibility(View.VISIBLE);
                }
            }
        });


        /*Get the action from spinner and do the task. Opens new fragment to get extra information based on the wanted action*/
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getChildFragmentManager();
                //Intent intent = new Intent(accountCreation.this, userApplication.class);
                String pinkoodi = pinni.getText().toString();
                System.out.println(pinkoodi + " " + cardPIN);
                message = "Toiminto onnistui!";


                if (!choice.equals("Valitse toiminto:") && pinkoodi.equals(cardPIN)) {
                    if (choice.equals("Maksa kortilla")) {
                        fragment_payCard pay_obj = (fragment_payCard) manager.findFragmentById(R.id.cardFragment);
                        String area= pay_obj.spinner.getSelectedItem().toString();
                        String amount =pay_obj.amout.getText().toString();
                        if (!amount.equals("")) {
                            double inputamount = Double.parseDouble(amount);
                            if (inputamount>0 && !area.equals("Valitse alue:")) {
                                pankki.cardPayment(card, pinkoodi, area, inputamount);
                            }
                        }
                    } else if (choice.equals("Nosta kortilta")) {
                        fragment_drawCard draw_obj = (fragment_drawCard) manager.findFragmentById(R.id.cardFragment);
                        String area= draw_obj.spinner.getSelectedItem().toString();
                        String amount = draw_obj.amount.getText().toString();
                        if (!amount.equals("")) {
                            double inputamount = Double.parseDouble(amount);
                            if (inputamount>0 && !area.equals("Valitse alue:")) {
                                pankki.takeCash(card, pinkoodi, area, inputamount);
                            }
                        }

                    }else if (choice.equals("Näytä kortin tiedot")) {
                        fragment_editCard edit_obj = (fragment_editCard) manager.findFragmentById(R.id.cardFragment);
                        updateCard(edit_obj);
                    } else {
                        Toast.makeText(getActivity(), "Tarkista alue ja summa", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getActivity(), "Väärä PIN", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /*Set actions to spinner*/
    public void setItemsOnSpinner() {
        list = new ArrayList<>();
        list.add("Valitse toiminto:");
        list.add("Maksa kortilla");
        list.add("Nosta kortilta");
        list.add("Näytä kortin tiedot");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /*Set cards on spinner*/
    public void setItemsOnSpinner2() {
        list2 = new ArrayList<>();
        list2.add("Valitse kortti:");
        for (int i=0; i < pankki.accountList.size(); i++) {
            if (userid.equals(pankki.accountList.get(i).getUserID())) {
                String accountid = pankki.accountList.get(i).getAccountID();
                for (int a=0; a < pankki.cardList.size();a++) {
                    if (accountid.equals(pankki.cardList.get(a).getAccountID())) {
                       list2.add(pankki.cardList.get(a).getCardNo());
                    }
                }
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list2);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
    }

    public void updateCard(fragment_editCard edit_obj) {
        String newArea = edit_obj.spinner.getSelectedItem().toString();
        String newDraw = edit_obj.newDraw.getText().toString();
        String newPay = edit_obj.newPay.getText().toString();
        String newPIN = edit_obj.newPin.getText().toString();
        String checkPIN = edit_obj.checkPin.getText().toString();
        CheckBox box = edit_obj.box;

        if (!newArea.equals("Valitse uusi alue:")) {
            passed = pankki.mDBHelper.cardSetArea(card,newArea);
        }
        if (!newDraw.equals("")) {
            double newDrawIn = Double.parseDouble(newDraw);
            passed = pankki.mDBHelper.cardSetCashLimit(card, newDrawIn);
        }
        if (!newPay.equals("")) {
            double newPayIn = Double.parseDouble(newPay);
            passed = pankki.mDBHelper.cardSetPayLimit(card, newPayIn);
        }
        if (!newPIN.equals("") && !checkPIN.equals("") && newPIN.equals(checkPIN)) {
            passed =  pankki.mDBHelper.cardSetPinCode(card, newPIN);
        }
        if (box.isChecked()) {
            pankki.mDBHelper.deleteCard(card);
            passed = true;
            message = "Kortti poistettu!";
        }
    }

    public void findPIN(){
        for (int a=0; a < pankki.cardList.size();a++) {
            if (card.equals(pankki.cardList.get(a).getCardNo())) {
                cardPIN = pankki.cardList.get(a).getPinCode();
            }
        }
    }
}
