package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class admin_etusivu extends Fragment {
    TextView header;
    TextView text;
    Button button;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.admin_etusivu, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        header = (TextView) view.findViewById(R.id.textView11);
        text = (TextView) view.findViewById(R.id.textView12);
        text.setMovementMethod(new ScrollingMovementMethod());
        button = (Button) view.findViewById(R.id.button9);
        Bank pankki = Bank.getBank();
        header.setText(pankki.getBankname());
        setText(pankki);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(getActivity(), signIn.class);
                intent.putExtra("message", "message");
                startActivity(intent);
            }
        });
    }

    private void setText(Bank pankki) {
        for (int i = 0; i < pankki.userList.size(); i++) {
            String id = pankki.userList.get(i).getUserID();
            int AC = getAccounts(id, pankki);
            int CA = getCards(id, pankki);
            text.setText(text.getText().toString()+ "Käyttäjä: " + id + "\n");
            text.setText(text.getText().toString()+ "Tilejä: " + AC + "\n");
            text.setText(text.getText().toString()+ "Kortteja: " + CA + "\n\n");
        }
    }

    private int getAccounts(String id, Bank pankki) {
        int AC = 0;
        for (int i = 0; i < pankki.accountList.size(); i++) {
            if (id.equals(pankki.accountList.get(i).getUserID())) {
                AC++;
            }
        }
        return AC;
    }

    private int getCards(String id, Bank pankki){
        int CA = 0;
        for (int i=0; i < pankki.accountList.size(); i++) {
            if (id.equals(pankki.accountList.get(i).getUserID())) {
                String accountid = pankki.accountList.get(i).getAccountID();
                for (int a = 0; a < pankki.cardList.size(); a++) {
                    if (accountid.equals(pankki.cardList.get(a).getAccountID())) {
                        CA++;
                    }
                }
            }
        }
        return CA;
    }
}
