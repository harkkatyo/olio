package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class userApplication extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String kayttaja;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_application);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView navFullName = (TextView) headerView.findViewById(R.id.fullName);
        Bank pankki = Bank.getBank();
        ArrayList <User> userList= pankki.userList;
        String name = "";
        pankki.mDBHelper.setUserContext(this);

        /*Take userID from logIn action*/

        if (getIntent().hasExtra("kayttaja")) {
            kayttaja = getIntent().getStringExtra("kayttaja");
            //System.out.println(kayttaja);
            for (int i=0; i < userList.size(); i++) {
                if (userList.get(i).getUserID().equals(kayttaja)) {
                    name = userList.get(i).getFirstName() + " " + userList.get(i).getLastName();
                }
            }
            navFullName.setText(name);
        }

        /*Take message from other actions*/
        if (getIntent().hasExtra("message")) {
            String viesti = getIntent().getStringExtra("message");
            Toast.makeText(userApplication.this, viesti, Toast.LENGTH_SHORT).show();
        }

        displaySelectedScreen(R.id.nav_menu);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        displaySelectedScreen(id);
        return true;
    }

    private void displaySelectedScreen(int id) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        bundle.putString("kayttajaID", kayttaja);

        if (id == R.id.nav_menu) {
            fragment = new fragment_main();
        } else if (id == R.id.nav_transfer) {
            fragment = new fragment_tilisiirto();
        } else if (id == R.id.nav_contacts) {
            fragment = new fragment_yhteystiedot();
        } else if (id == R.id.nav_save) {
            fragment = new fragment_talletus();
        } else if (id == R.id.nav_withdrawal) {
            fragment = new fragment_nosto();
        } else if (id == R.id.nav_card) {
            fragment = new fragment_korttitoiminnot();
        } else if (id == R.id.nav_transaction) {
            fragment = new fragment_transactions();
        }
        fragment.setArguments(bundle);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentWindow, fragment);
        ft.commit();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logOut) {
            Intent intent = new Intent(userApplication.this, MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
