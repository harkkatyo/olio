package com.example.harkkatyo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Entry {
    Date dateTime;
    String sendAccount;
    String getAccount;
    String method;
    String sum;
    String strDateTime;

    public Entry (Date new_dateTime, String new_sendAccount, String new_getAccount, String new_method, double new_sum) {
        dateTime = new_dateTime;
        sendAccount = new_sendAccount;
        getAccount = new_getAccount;
        method = new_method;
        sum = Double.toString(Math.abs(new_sum));
    }

    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Helsinki"));
        strDateTime = dateFormat.format(dateTime);
        return strDateTime;
    }

    public String getSendAccount() { return sendAccount;
    }

    public String getGetAccount() { return getAccount;
    }

    public String getMethod() { return method;
    }

    public String getSum() { return sum;
    }
}
